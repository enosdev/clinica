<?php $v->layout("_login"); ?>

<!-- [ Side container ] Start -->
<!-- Do not display the container on extra small, small and medium screens -->
<div class="d-none d-lg-flex col-lg-8 align-items-center ui-bg-cover ui-bg-overlay-container p-5" style="background-image: url('<?= theme("/assets/img/gestao.jpg", CONF_VIEW_ADMIN);?>');">
    <div class="ui-bg-overlay bg-dark opacity-50"></div>
    <!-- [ Text ] Start -->
    <div class="w-100 text-white px-5">
        <h1 class="display-2 font-weight-bolder mb-4">SISTEMA DE<br>GERENCIAMENTO</h1>
        <div class="text-large font-weight-light">
            <strong>Estudio Fox</strong> apresenta a nova ferramenta de Gestão de Conteúdo com interface amigável e completamente dinâmica!<br />
            Nosso sistema é atualizado periodicamente em questão de segurança e usabilidade sem nenhum custo adicional
        </div>
    </div>
    <!-- [ Text ] End -->
</div>
<!-- [ Side container ] End -->

<!-- [ Form container ] Start -->
<div class="d-flex col-lg-4 align-items-center bg-white p-5">
    <!-- Inner container -->
    <!-- Have to add `.d-flex` to control width via `.col-*` classes -->
    <div class="d-flex col-sm-7 col-md-5 col-lg-12 px-0 px-xl-4 mx-auto">
        <div class="w-100">

            <!-- [ Logo ] Start -->
            <div class="d-flex justify-content-center align-items-center">
                <div class="ui-w-60">
                    <div class="w-100 position-relative">
                        <img src="<?= theme("/assets/img/logoface.png", CONF_VIEW_ADMIN);?>" alt="Fox Logo" class="img-fluid">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- [ Logo ] End -->

            <h4 class="text-center text-lighter font-weight-normal mt-5 mb-0">Faça Login na Sua Conta</h4>

            <!-- [ Form ] Start -->
            <form class="my-5">
                <div class="ajax_response"><?= flash(); ?></div>
                <div class="form-group">
                    <label class="form-label">Email</label>
                    <input type="email" class="form-control" name="email">
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="form-label d-flex justify-content-between align-items-end">
                        <span>Senha</span>
                        <a href="#" class="d-block small">Esqueceu sua senha?</a>
                    </label>
                    <input type="password" class="form-control" name="password">
                    <div class="clearfix"></div>
                </div>
                <div class="d-flex justify-content-between align-items-center m-0">
                    <label class="custom-control custom-checkbox m-0">
                        <!-- <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-label">Remember me</span> -->
                    </label>
                    <button class="btn btn-primary">Entrar</button>
                </div>
            </form>
            <!-- [ Form ] End -->

            <div class="text-center text-muted">
                Desenvolvido por www.<strong>estudiofox</strong>.com<br />
                &copy; 2020 - todos os direitos reservados<br />
                <a target="_blank" class="link_whats" href="https://api.whatsapp.com/send?phone=5573998690564&text=Olá, preciso de ajuda com o login no sistema <?=CONF_SITE_NAME;?>.">
                    <i class="fab fa-whatsapp"></i> WhatsApp: (73) 99869 0564
                </a>
            </div>

        </div>
    </div>
</div>
<!-- [ Form container ] End -->