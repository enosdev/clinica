<?php $v->layout("_admin");
    /**
     * nível de usuários
     * 5 super admin
     * 6 admin comum
     * 7 gerencia
     * 8 editor recepção
     * 9 dentista
     * 10 negociadora
     */
?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Visualização de Clientes</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active">clientes</li>
        </ol>
    </div>

    <?php if (!$clients): ?>
    <div class="card mb-4">
        <h6 class="card-header">Adicionar Cliente</h6>
        <div class="card-body text-muted">
            <form action="<?= url("/".PATH_ADMIN."/clients/client"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>
                <input type="hidden" name="id_user" value="<?=user()->id;?>"/>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">*Nome:</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Primeiro nome" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Sobrenome:</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Último nome" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Nascimento:</label>
                        <input type="text" name="datebirth" class="form-control mask-date" placeholder="dd/mm/yyyy">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Genero:</label>
                        <select name="genre" class="custom-select" required>
                            <option selected></option>
                            <option value="male">Masculino</option>
                            <option value="female">Feminino</option>
                            <option value="other">Outros</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*CPF:</label>
                        <input type="text" class="form-control mask-doc" name="document" placeholder="CPF do Cliente">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Identidade:</label>
                        <input type="text" class="form-control" name="identity" placeholder="Carteira de identidade">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Celular:</label>
                        <input type="text" class="form-control mask-phone" name="phone" placeholder="Número do celular" required>
                        <small class="form-text text-muted">Número deve conter o DDD Ex: 73 9 0000-0000.</small>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Celular recado:</label>
                        <input type="text" class="form-control mask-phone" name="phone2" placeholder="Número do celular de recado">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">E-mail:</label>
                        <input type="email" class="form-control" name="email" placeholder="Melhor e-mail">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Instagram:</label>
                        <input type="text" class="form-control" name="instagram" placeholder="@enos.fox">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Facebook:</label>
                        <input type="text" class="form-control" name="facebook" placeholder="usuario_face">
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row d-none">
                    <div class="form-group col-md-12">
                    <label class="form-label">Descrições e Observações:</label>
                        <textarea class="form-control mce" rows="15" name="description"></textarea>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto paciente: <small>Obrigatório foto</small></label>
                        <input type="file" name="photo_client" accept="image/*">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto frente:</label>
                        <input type="file" name="photo" accept="image/*">
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto verso:</label>
                        <input type="file" name="photo2" accept="image/*">
                    </div>
                </div>
                <hr>
                <button class="btn btn-success">Add Cliente</button>
            </form>
        </div>
    </div>

    <?php else: ?>

    <div class="card mb-4">
        <div class="card-header">
        <?php if(user()->level == 5):?>
        <?php if(!$clients->card()): ?>
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal-prontuario">Criar um Prontuário para <strong><?=$clients->fullName();?></strong></button>
        <?php else:?>
            <a href="<?= url("/".PATH_ADMIN."/cards/card/{$clients->card()->id}"); ?>" class="btn btn-info btn-lg">ver Prontuário de <strong><?=$clients->fullName();?></strong></a>
        <?php endif;?>
        <?php else:?>
            <div class="alert alert-danger">
                sem autorização para criar prontuário
            </div>
        <?php endif;?>
            <!-- <a href="#" class="btn btn-success btn-lg">Criar uma ficha para <strong><?=$clients->fullName();?></strong></a> -->
        </div>
        <div class="card-body text-muted">
            <form action="<?= url("/".PATH_ADMIN."/clients/client/{$clients->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">*Nome:</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Primeiro nome" value="<?=$clients->first_name;?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Sobrenome:</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Último nome" value="<?=$clients->last_name;?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Nascimento:</label>
                        <input type="text" name="datebirth" class="form-control mask-date" value="<?= date_fmt($clients->datebirth, "d/m/Y"); ?>" placeholder="dd/mm/yyyy" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Genero:</label>
                        <select name="genre" class="custom-select" required>
                            <?php
                            $genre = $clients->genre;
                            $select = function ($value) use ($genre) {
                                return ($genre == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("male"); ?> value="male">Masculino</option>
                            <option <?= $select("female"); ?> value="female">Feminino</option>
                            <option <?= $select("other"); ?> value="other">Outros</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*CPF:</label>
                        <input type="text" class="form-control mask-doc" name="document" value="<?=$clients->document;?>" placeholder="CPF do Cliente" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Identidade:</label>
                        <input type="text" class="form-control" name="identity" value="<?=$clients->identity;?>" placeholder="Carteira de identidade">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Celular:</label>
                        <input type="text" class="form-control mask-phone" name="phone" value="<?=$clients->phone;?>" placeholder="Número do celular" required>
                        <small class="form-text text-muted">Número deve conter o DDD Ex: 73 9 0000-0000.</small>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Celular recado:</label>
                        <input type="text" class="form-control mask-phone" name="phone2" value="<?=$clients->phone2;?>" placeholder="Número do celular de recado">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">E-mail:</label>
                        <input type="email" class="form-control" name="email" value="<?=$clients->email;?>" placeholder="Melhor e-mail">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Instagram:</label>
                        <input type="text" class="form-control" name="instagram" value="<?=$clients->instagram;?>" placeholder="@enos.fox">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Facebook:</label>
                        <input type="text" class="form-control" name="facebook" value="<?=$clients->facebook;?>" placeholder="usuario_face">
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row d-none">
                    <div class="form-group col-md-12">
                        <label class="form-label">Descrições e Observações:</label>
                        <textarea class="form-control mce" rows="15" name="description"><?=$clients->description;?></textarea>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto paciente: <small>Obrigatório foto</small></label>
                        <input type="file" name="photo_client" accept="image/*">
                        <div class="mt-2">
                            <img src="<?= image($clients->photo_client, 400); ?>" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto frente:</label>
                        <input type="file" name="photo" accept="image/*">
                        <div class="clearfix"></div>
                        <div class="mt-2">
                            <img src="<?= image($clients->photo, 400); ?>" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto verso:</label>
                        <input type="file" name="photo2" accept="image/*">
                        <div class="clearfix"></div>
                        <div class="mt-2">
                            <img src="<?= image($clients->photo2, 400); ?>" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
                <hr>
                <button class="btn btn-info">Atualizar</button>
                <a href="#" class="btn btn-dark"
                       data-post="<?= url("/".PATH_ADMIN."/clients/client/{$clients->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir o Cliente? Essa ação não pode ser desfeita!"
                       data-user_id="<?= $clients->id; ?>">Excluir Cliente</a>
            </form>
        </div>
    </div>

    <!-- modal inserir prontuário -->
    <div class="modal fade" id="modal-prontuario" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Gerar um protuário</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= url("/".PATH_ADMIN."/cards/card"); ?>" method="post">
                        <input type="hidden" name="client" value="<?=$clients->id;?>">
                        <input type="hidden" name="action" value="create"/>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label">Add para <span class="name-client" style="font-style:italic"><?=$clients->fullName();?></span>:</label>
                                        <input id="numeros" type="number" class="form-control" name="registry" value="" placeholder="inserir número">
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-success">Criar Prontuário</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php endif; ?>

</div>

<?php $v->start("scripts");?>
    <script src="<?= url("/shared/scripts/jquery.mask.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/tinymce/tinymce.min.js"); ?>"></script>
    <script>

        $(function(){
            $('.mask-date').mask('00/00/0000');
            $('.mask-doc').mask('000.000.000-00', { reverse: true });
            $('.mask-phone').mask('(00) 0 0000-0000');
        });

    tinymce.init({
        selector: 'textarea.mce',
        language: 'pt_BR',
        content_style: 'body { font-size: .8em; }',
        plugins:
            'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        // imagetools_cors_hosts: ['picsum.photos'],
        // menubar: 'file edit view insert format tools table help',
        menubar: false,
        toolbar: 'undo redo | formatselect | ' +
                'bold italic underline backcolor forecolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat',
        // toolbar:
        //     'undo redo | bold italic underline strikethrough | formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap | fullscreen  preview print | insertfile image media link | ltr rtl | esdimg esdfile',
        // mobile: {
        //     theme: 'mobile',
        //     plugins: ['paste', 'lists', 'autolink', 'image', 'link', 'media', 'imagetools', ''],
        //     toolbar: ['undo', 'bold', 'italic', 'styleselect', 'forecolor', 'esdimg', 'esdfile', 'image', 'media', 'link']
        // },
        toolbar_sticky: true,
        image_advtab: true,
        image_class_list: [{ title: 'None', value: '' }, { title: 'Some class', value: 'class-name' }],
        importcss_append: true,
        template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
        height: 300,
        image_caption: true,
        relative_urls: false,
        convert_urls: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: 'mceNonEditable',
        toolbar_drawer: 'sliding',
        contextmenu: 'link image imagetools table',

    });

    </script>
<?php $v->end();?>