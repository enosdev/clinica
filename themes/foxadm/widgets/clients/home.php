<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Gerenciamento de Clientes</h4>

    <!-- liveline-section start -->
    <div class="col-sm-12">
        <div class="card text-right">
            <div class="card-body text-center">
                <div class="row align-items-center m-l-0">
                    <div class="col-md">
                        <form class="form-group" action="<?= url("/".PATH_ADMIN."/clients/home"); ?>">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar Cliente" name="s" value="<?= $search; ?>">
                                <span class="input-group-append">
                                    <button class="btn btn-primary">Buscar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if(!$clients):?>
        <div class="alert alert-info alert-dismissible fade show">
            <i class="fas fa-info"></i> Ainda não existem Clientes cadastrados.
        </div>
    <?php else:?>
        <div class="xrow">
            <!-- customar project  start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center m-l-0 text-muted">Total cadastrados: <div class="text-danger ml-2"><?=$count;?></div></div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>Foto</th>
                                        <th>Nome</th>
                                        <th>Email</th>
                                        <th>Telefone</th>
                                        <th>Prontuário</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($clients as $cli):
                                        $clientPhoto = ($cli->photo() ? image($cli->photo_client, 40, 40) :
                                        image("/img.jpg", 40, 40));
                                    ?>
                                    <tr>
                                        <td>
                                            <img src="<?=$clientPhoto;?>" class="img-fluid img-radius wid-40" alt="">
                                        </td>
                                        <td><?= $cli->fullName(); ?></td>
                                        <td><?= $cli->email; ?></td>
                                        <td><?= $cli->phone; ?></td>
                                        <td>
                                        <?php if(user()->level == 5):?>
                                        <?php if(!$cli->card()): ?>
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-prontuario"
                                            data-client="<?=$cli->id;?>"
                                            data-name="<?= $cli->fullName(); ?>">Criar Prontuário</button>
                                        <?php else:?>
                                            <a href="<?= url("/".PATH_ADMIN."/cards/card/{$cli->card()->id}"); ?>" class="btn btn-success btn-sm">Ver</a>
                                        <?php endif;?>

                                        <?php else:?>
                                            <div class="alert alert-danger">Pendende</div>
                                        <?php endif;?>
                                        </td>
                                        <td>
                                            <a href="<?= url("/".PATH_ADMIN."/clients/client/{$cli->id}"); ?>" class="btn btn-info btn-sm">Gerenciar </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customar project  end -->
        </div>
        
        <div class="row paginacao">
            <?=$paginator;?>
        </div>
        

    <?php endif;?>
    
    <!-- liveline-section end -->
</div>

<!-- modal inserir prontuário -->
<div class="modal fade" id="modal-prontuario" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gerar um protuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= url("/".PATH_ADMIN."/cards/card"); ?>" method="post">
                    <input type="hidden" name="client" value="cliente">
                    <input type="hidden" name="action" value="create"/>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Add para <span class="name-client" style="font-style:italic">fulado de tal</span>:</label>
                                    <input id="numeros" type="number" class="form-control" name="registry" value="" placeholder="inserir número">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success">Criar Prontuário</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $v->start("scripts");?>
<script>
    ((win,doc)=>{

            //inserindo informações do cliente no formulários
            const clients = doc.querySelectorAll('[data-target="#modal-prontuario"]')
            clients.forEach(att_data => att_data.addEventListener('click', () => {
                let name = att_data.dataset.name
                let client = att_data.dataset.client
                doc.querySelector('[name="client"]').value = client
                doc.querySelector('.name-client').innerHTML = name
            }))


            //restringir digitar somente numeros no form criar prontuário
            doc.querySelector("#numeros").onkeypress = () =>{
                if( event.charCode >= 48 && event.charCode <= 57 ){
                    return event.charCode >= 48 && event.charCode <= 57
                } else {
                    alert('Favor digitar neste campo somente números')
                    return event.charCode >= 48 && event.charCode <= 57     
                }
            }
        
    })(window,document)
</script>
<?php $v->end();?>