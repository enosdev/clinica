<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Gerenciamento de Usuários</h4>
    <div class="row justify-content-center">

        <!-- liveline-section start -->
        <div class="col-sm-12">
            <div class="card text-right">
                <div class="card-body text-center">
                    <div class="row align-items-center m-l-0">
                        <div class="col-md">
                            <form class="form-group" action="<?= url("/".PATH_ADMIN."/users/home"); ?>">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Buscar Usuário" name="s" value="<?= $search; ?>">
                                    <span class="input-group-append">
                                        <button class="btn btn-primary">Buscar</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php foreach ($users as $user):
        $userPhoto = ($user->photo() ? image($user->photo, 80, 80) :
            theme("/assets/img/avatar.jpg", CONF_VIEW_ADMIN));
        ?>
        <div class="col-lg-4 col-md-6">
            <div class="card user-card user-card-1 mt-4">
                <div class="card-body">
                    <div class="user-about-block text-center">
                        <div class="row align-items-start">
                            <div class="col text-left pb-3">
                            <?php if($user->status == 'confirmed'): ?>
                                <span class="badge badge-success">Ativo</span>
                            <?php else: ?>
                                <span class="badge badge-danger">Inativo</span>
                            <?php endif; ?>
                            </div>
                            <div class="col"><img class="img-radius img-fluid wid-80" src="<?=$userPhoto;?>" alt="User image"></div>
                            <div class="col text-right pb-3">
                                <div class="dropdown">
                                    <a href="<?= url("/".PATH_ADMIN."/users/user/{$user->id}"); ?>" class="btn btn-info text-white btn-sm">Gerenciar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="#!" data-toggle="modal" data-target="#modal-report">
                            <h4 class="mb-1 mt-3"><?= $user->fullName(); ?></h4>
                        </a>
                        <p class="mb-3 text-muted"><i class="feather icon-calendar"> </i> <?=date("d/m/Y",strtotime($user->datebirth));?></p>
                        <p class="mb-1"><b>Email : </b><?=$user->email;?></p>
                        <p class="mb-0"><b>Usuário : </b>
                        <?php if ($user->level == 5): ?>
                            <i class="fa fa-laptop-code"></i> Web Master
                        <?php elseif ($user->level == 6): ?>
                            <i class="fa fa-users-cog"></i> Gerencia
                        <?php elseif ($user->level == 7): ?>
                            <i class="fa fa-people-carry"></i> Recepção
                        <?php elseif ($user->level == 8): ?>
                            <i class="fa fa-user-md"></i> Dentista
                        <?php else: ?>
                            <i class="fa fa-dollar-sign"></i> Negociador
                        <?php endif; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <!-- liveline-section end -->
    </div>
</div>

<!-- 
    * 6 gerencia
    * 7 Recepção
    * 8 dentista
    * 9 Negociador
 -->