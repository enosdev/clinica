<?php $v->layout("_admin");
    /**
     * nível de usuários
     * 5 super admin
     * 6 gerencia
     * 7 Recepção
     * 8 dentista
     * 9 Negociador
     */
?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Visualização de usuários</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active">visualização de usuários</li>
        </ol>
    </div>

    <?php if (!$user):
        if(user()->level > 6){
            header('Location: '.url("/".PATH_ADMIN));
        }    
    ?>

    <div class="media align-items-center py-3 mb-3">
        <div id="image-holder">
            <img src="<?=theme("/assets/img/avatar.jpg", CONF_VIEW_ADMIN);?>" alt="" class="d-block ui-w-100 rounded-circle">
        </div>
        <div class="media-body ml-4">
            <h4 class="font-weight-bold mb-0">foto<span class="text-muted font-weight-normal">@usuário</span></h4>
        </div>
    </div>

    <div class="card mb-4">
        <h6 class="card-header">Novo Usuário</h6>
        <div class="card-body">
            <form action="<?= url("/".PATH_ADMIN."/users/user"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">*Nome:</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Primeiro nome" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Sobrenome:</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Último nome" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Genero:</label>
                        <select name="genre" class="custom-select" required>
                            <option>Selecione</option>
                            <option value="male">Masculino</option>
                            <option value="female">Feminino</option>
                            <option value="other">Outros</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">Nascimento:</label>
                        <input type="text" name="datebirth" class="form-control mask-date" placeholder="dd/mm/yyyy">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">CPF:</label>
                        <input type="text" class="form-control mask-doc" name="document" placeholder="CPF do usuário">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">RG:</label>
                        <input type="text" class="form-control" name="identity" placeholder="Carteira de Identidade">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Telefone:</label>
                        <input type="text" class="form-control mask-phone" name="phone" placeholder="Contato Celular">
                        <small class="form-text text-muted">Número deve conter o DDD Ex: 73 9 0000-0000.</small>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Data de Admissão:</label>
                        <input type="text" class="form-control mask-date" name="admission" placeholder="Dia que entrou na empresa">
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="form-group col-md-4">
                        <label class="form-label">*E-mail:</label>
                        <input type="email" class="form-control" name="email" placeholder="Melhor e-mail" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Senha:</label>
                        <input type="password" name="password" class="form-control" placeholder="Senha de acesso" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">*Nível de Acesso:</label>
                        <select name="level" class="custom-select" required>
                            <?php
                            if ( user()->level == 5) {
                                ?>
                                <option value="5">Super Admin</option>
                                <?php
                            }
                            ?>
                            <!-- <option value="1">Usuário</option> -->
                            <option value="6">Gerencia</option>
                            <option value="7">Recepção</option>
                            <option value="8">Dentista</option>
                            <option value="9">Negociador</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 doctor d-none">
                        <label class="form-label">*CRO:</label>
                        <input type="text" class="form-control" name="cro" placeholder="CRO do dentista">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4 doctor d-none">
                        <label class="form-label">*Comissão:</label>
                        <input type="text" class="form-control" name="commission" placeholder="Procentagem da comissão">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Status:</label>
                        <select name="status" class="custom-select" required>
                            <option value="confirmed">Ativo</option>
                            <option value="registered">Inativo</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 status d-none">
                        <label class="form-label">Data de Saída:</label>
                        <input type="text" class="form-control mask-date" name="exit_company" placeholder="Dia que saiu">
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label w-100">Foto:</label>
                    <input id="file" type="file" name="photo" accept="image/*">
                    <small class="form-text text-muted">Ideal seria a imagem quadrada de 400 x 400px.</small>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">Permissão:</label>
                        <div class="clearfix"></div>
                    </div>
                    <?php foreach($permission as $permit):?>
                        <div class="form-group col-md-2">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" name="permission[]" value="<?=$permit->uri;?>" class="custom-control-input">
                                <span class="custom-control-label"><?=$permit->title;?></span>
                            </label>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-success">Criar usuário</button>
            </form>
        </div>
    </div>

    <?php else: ?>
    
    <div class="media align-items-center py-3 mb-3">
        <div id="image-holder">
            <img src="<?= image($user->photo, 100, 100); ?>" alt="" class="d-block ui-w-100 rounded-circle">
        </div>
        <div class="media-body ml-4">
            <h4 class="font-weight-bold mb-0"><?= $user->first_name; ?><span class="text-muted font-weight-normal"> <?= $user->last_name; ?></span></h4>
            <div class="text-muted mb-2">ID: <?= $user->id; ?></div>
        </div>
    </div>

    <div class="card mb-4">
        <h6 class="card-header">Novo Usuário</h6>
        <div class="card-body">
            <form action="<?= url("/".PATH_ADMIN."/users/user/{$user->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">*Nome:</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Primeiro nome" value="<?= $user->first_name; ?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Sobrenome:</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Último nome" value="<?= $user->last_name; ?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Genero:</label>
                        <select name="genre" class="custom-select">
                            <?php
                            $genre = $user->genre;
                            $select = function ($value) use ($genre) {
                                return ($genre == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("male"); ?> value="male">Masculino</option>
                            <option <?= $select("female"); ?> value="female">Feminino</option>
                            <option <?= $select("other"); ?> value="other">Outros</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label class="form-label">Nascimento:</label>
                        <input type="text" name="datebirth" class="form-control mask-date" value="<?= date_fmt($user->datebirth, "d/m/Y"); ?>" placeholder="dd/mm/yyyy">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">CPF:</label>
                        <input type="text" class="form-control mask-doc" name="document" value="<?= $user->document; ?>" placeholder="CPF do usuário">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">RG:</label>
                        <input type="text" class="form-control" name="identity" value="<?= $user->identity;?>" placeholder="Carteira de Identidade">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Telefone:</label>
                        <input type="text" class="form-control mask-phone" name="phone" value="<?= $user->phone;?>" placeholder="Contato Celular">
                        <small class="form-text text-muted">Número deve conter o DDD Ex: 73 9 0000-0000.</small>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Data de Admissão:</label>
                        <input type="text" class="form-control mask-date" name="admission" value="<?= date_fmt($user->admission, "d/m/Y");?>" placeholder="Dia que entrou na empresa">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">*E-mail:</label>
                        <input type="email" class="form-control" name="email" value="<?= $user->email; ?>" placeholder="Melhor e-mail" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="form-label">Senha:</label>
                        <input type="password" name="password" class="form-control" placeholder="Senha de acesso">
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <?php if ( user()->level <= 6):?>

                    <div class="form-group col-md-4">
                        <label class="form-label">*Nível de Acesso:</label>
                        <select name="level" class="custom-select" required>
                            <?php
                            $level = $user->level;
                            $select = function ($value) use ($level) {
                                return ($level == $value ? "selected" : "");
                            };
                            if ( user()->level == 5){
                                ?>
                                <option <?= $select(5); ?> value="5">Super Admin</option>
                            <?php
                            }
                            ?>
                            <!-- <option < ?= $select(1); ?> value="1">Usuário</option> -->
                            <option <?= $select(6); ?> value="6">Gerencia</option>
                            <option <?= $select(7); ?> value="7">Recepção</option>
                            <option <?= $select(8); ?> value="8">Dentista</option>
                            <option <?= $select(9); ?> value="9">Negociador</option>
                        </select>
                    </div>

                    <?php endif;?>


                    <div class="form-group col-md-4 doctor d-none">
                        <label class="form-label">*CRO:</label>
                        <input type="text" class="form-control" name="cro" placeholder="CRO do dentista" value="<?=$user->cro;?>">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-4 doctor d-none">
                        <label class="form-label">*Comissão:</label>
                        <input type="text" class="form-control" name="commission" placeholder="Procentagem da comissão" value="<?=$user->commission;?>">
                        <div class="clearfix"></div>
                    </div>

                    <?php if( user()->level <= 6):?>
                    <div class="form-group col-md-4">
                        <label class="form-label">*Status:</label>
                        <select name="status" class="custom-select" required>
                            <?php
                            $status = $user->status;
                            $select = function ($value) use ($status) {
                                return ($status == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("registered"); ?> value="registered">Inativo</option>
                            <option <?= $select("confirmed"); ?> value="confirmed">Ativo</option>
                        </select>
                    </div>
                    <?php endif;?>
                    <div class="form-group col-md-4 status d-none">
                        <label class="form-label">Data de Saída:</label>
                        <input type="text" class="form-control mask-date" name="exit_company" value="<?=$user->exit_company;?>" placeholder="Dia que saiu">
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label w-100">Foto:</label>
                    <input id="file" type="file" name="photo" accept="image/*">
                    <small class="form-text text-muted">Ideal seria a imagem quadrada de 400 x 400px.</small>
                </div>

                <?php if( user()->level <= 6):?>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">Permissão:</label>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                        $pag = explode(',',$user->permission);
                        $checked = function ($value) use ($pag) {
                            return ((in_array($value, $pag)) ? "checked" : "");
                        };    
                        foreach($permission as $permit):?>
                            <div class="form-group col-md-2">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="permission[]" value="<?=$permit->uri;?>" <?= $checked($permit->uri); ?> class="custom-control-input">
                                    <span class="custom-control-label"><?=$permit->title;?></span>
                                </label>
                            </div>
                    <?php endforeach;?>
                </div>
                <?php endif;?>
                <div class="clearfix"></div>
                <button class="btn btn-info">Atualizar</button>
                <?php if(user()->level == 5):?>
                    <button class="btn btn-dark"
                       data-post="<?= url("/".PATH_ADMIN."/users/user/{$user->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir o usuário e todos os dados relacionados a ele? Essa ação não pode ser desfeita!"
                       data-user_id="<?= $user->id; ?>">Excluir Usuário</button>
                <?php endif;?>
            </form>
        </div>
    </div>

    <?php endif; ?>

</div>

<?php $v->start("scripts");?>
    <script src="<?= url("/shared/scripts/jquery.mask.js"); ?>"></script>
    <script>
        $(function(){
            //mascaras
            $('.mask-date').mask('00/00/0000');
            $('.mask-doc').mask('000.000.000-00', { reverse: true });
            $('.mask-phone').mask('(00) 0 0000-0000');

            //select de dentista
            $('select[name="level"]').change(function(){
                var valor = $(this).val();
                if(valor == 8){
                    $('.doctor').removeClass('d-none');
                } else {
                    $('.doctor').addClass('d-none');
                    $('.doctor input').val('');
                }
            });

            //verifica qual está selecionado ao abrir a página
            var selected = $('select[name="level"] option:selected').val();
            if(selected == 8){
                $('.doctor').removeClass('d-none');
            }

            //select de status
            $('select[name="status"]').change(function(){
                var valor = $(this).val();
                if(valor == 'registered'){
                    $('.status').removeClass('d-none');
                } else {
                    $('.status').addClass('d-none');
                    $('.doctor input').val('');
                }
            });

            //verifica qual está selecionado ao abrir a página
            var selected = $('select[name="status"] option:selected').val();
            if(selected == 'registered'){
                $('.status').removeClass('d-none');
            }
        });
    </script>
<?php $v->end();?>