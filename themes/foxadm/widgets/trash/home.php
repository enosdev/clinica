<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Configuração</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-settings"></i></a></li>
            <li class="breadcrumb-item active" >Configurações</li>
        </ol>
    </div>

    <hr class="border-light container-m--x my-4">
    <div class="text-white font-weight-bold p-3 bg-danger">Artigos</div>

    <div class="row px-3 py-1">
        <?php if (!$posts): ?>
            <div class="alert alert-info alert-dismissible fade show">
                <i class="fas fa-info"></i> Ainda não existem artigos no lixo.
            </div>
        <?php else: ?>
            <?php foreach ($posts as $post):
                $postCover = ($post->cover ? image($post->cover, 200) : "");
            ?>
            <div class="card col-md-3 p-0">
                <img class="card-img-top img-fluid" src="<?=$postCover;?>" alt="imagem">
                <div class="card-body">
                    <h4 class="card-title"><a target="_blank" href=" <?= url("/artigo/{$post->uri}"); ?>" title="Ver no site"><?= $post->title; ?></a></h4>
                    <p class="card-text" style="color:<?=$post->category()->color;?>;"><i class="far fa-tag"></i> <?= $post->category()->title; ?></p>
                    <a class="btn btn-info" title=""
                    href="<?= url("/".PATH_ADMIN."/blog/post/{$post->id}"); ?>">Editar</a>
                    <a class="btn btn-danger" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/blog/post"); ?>"
                    data-action="delete"
                    data-confirm="Tem certeza que deseja deletar este post definitivamente?"
                    data-post_id="<?= $post->id; ?>">Deletar</a>
                </div>
            </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <hr class="border-light container-m--x my-4">
    <div class="text-white font-weight-bold p-3 bg-danger">Colunas</div>

    <div class="row px-3 py-1">
        <?php if (!$column): ?>
            <div class="alert alert-info alert-dismissible fade show">
                <i class="fas fa-info"></i> Ainda não existem Colunas no lixo.
            </div>
        <?php else: ?>
            <?php foreach ($column as $post):
                $postCover = ($post->cover ? image($post->cover, 200) : "");
            ?>
            <div class="card col-md-3 p-0">
                <img class="card-img-top img-fluid" src="<?=$postCover;?>" alt="imagem">
                <div class="card-body">
                    <h4 class="card-title"><a target="_blank" href=" <?= url("/artigo/{$post->uri}"); ?>" title="Ver no site"><?= $post->title; ?></a></h4>
                    <p class="card-text" style="color:<?=$post->category()->color;?>;"><i class="far fa-tag"></i> <?= $post->category()->title; ?></p>
                    <a class="btn btn-info" title=""
                    href="<?= url("/".PATH_ADMIN."/column/post/{$post->id}"); ?>">Editar</a>
                    <a class="btn btn-danger" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/column/post"); ?>"
                    data-action="delete"
                    data-confirm="Tem certeza que deseja deletar este post definitivamente?"
                    data-post_id="<?= $post->id; ?>">Deletar</a>
                </div>
            </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>


</div>