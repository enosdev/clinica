<!-- Modal Incert Orçamento -->
<div class="modal fade" id="modal-report" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Enviar para Orçamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= url("/".PATH_ADMIN."/cards/card"); ?>" method="post">
                    <input type="hidden" name="action" value="create_budget"/>
                    <input type="hidden" name="card" value="<?=$cards->id;?>"/>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Department">Dentista</label>
                                <select class="form-control" id="Department" name="user" required>
                                    <option value="">Escolha um dentista</option>
                                    <?php if($doctor):
                                        foreach($doctor as $d):
                                    ?>
                                    <option value="<?=$d->id;?>"><?=$d->fullName();?></option>
                                    <?php endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col sm 6">
                            <div class="form-group">
                                <label class="form-label">Agendar horário para <span style="font-style:italic"><?=$cards->client()->first_name;?></span>:</label>
                                <input type="text" class="form-control datetimepicker" name="sheduled_at" value="" required>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success">Enviar para Orçamento</button>
                </form>

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="mt-3">Horários</h5>
                    </div>
                    <div class="col-sm-12 result">
                        <div class="alert alert-info alert-dismissible fade show">
                            <i class="fas fa-info"></i> Ainda não existe Agendamento.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Incert Agendament de procedimento -->
<div class="modal fade" id="modal-agendar" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agendar o procedimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= url("/".PATH_ADMIN."/cards/card"); ?>" method="post">
                    <input type="hidden" name="action" value="create_shedule_procedure"/>
                    <input type="hidden" name="card" value="<?=$cards->id;?>"/>
                    <input data-sp="shedule-procedure" type="hidden" name="procedure" value=""/>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Department">Dentista</label>
                                <select class="form-control" id="Department" name="user" required>
                                    <option value="">Escolha um dentista</option>
                                    <?php if($doctor):
                                        foreach($doctor as $d):
                                    ?>
                                    <option value="<?=$d->id;?>"><?=$d->fullName();?></option>
                                    <?php endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col sm 6">
                            <div class="form-group">
                                <label class="form-label">Agendar horário para <span style="font-style:italic"><?=$cards->client()->first_name;?></span>:</label>
                                <input type="text" class="form-control datetimepicker" name="sheduled_at" value="" required>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success">+ Add Agendamento</button>
                </form>

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="mt-3">Horários</h5>
                    </div>
                    <div class="col-sm-12 result">
                        <div class="alert alert-info alert-dismissible fade show">
                            <i class="fas fa-info"></i> Ainda não existe Agendamento.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Incert Procedure -->
<div class="modal fade" id="modal-procedure" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inserir Procedimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-4">
                    <div>Ativar Odontograma</div>
                    <input type="radio" name="odontograma" value="yes" id="yes"> <label for="yes">Ativo</label>
                    <input type="radio" name="odontograma" value="no" id="no" checked> <label for="no">Desativado</label>
                </div>
                <form action="<?= url("/".PATH_ADMIN."/cards/modal"); ?>" method="post">
                    <input type="hidden" name="action" value="create_procedures_sheet"/>
                    <input type="hidden" name="card" value="<?=$cards->id;?>"/>
                    <input type="hidden" name="user" value="<?=user()->id;?>"/>
                    <style>
                        .dentes .col img{
                            width:100%
                        }
                    </style>
                    <div class="row">
                        <div class="d-none odontograma">
                            <div class="col-md-12">
                            <label class="floating-label">Odontograma</label>
                            </div>
                            <div class="row no-gutters dentes mb-3">
                                <?php
                                    $teeth = [18,17,16,15,14,13,12,11,21,22,23,24,25,26,27,28];
                                    foreach($teeth as $tooth): ?>
                                            <div class="col flex-column px-2">
                                            <img src="<?=theme("/assets/odontograma/{$tooth}.png",CONF_VIEW_ADMIN);?>" alt="">
                                        </div>
                                <?php endforeach;?>
                            </div>
                            <div class="row no-gutters dentes mt-3">
                                <?php
                                    $teeth2 = [48,47,46,45,44,43,42,41,31,32,33,34,35,36,37,38];
                                    foreach($teeth2 as $tooth): ?>
                                            <div class="col flex-column px-2">
                                            <img src="<?=theme("/assets/odontograma/{$tooth}.png",CONF_VIEW_ADMIN);?>" alt="">
                                        </div>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row html-copy">
                                <div class="col-6">
                                    <label class="floating-label">Procedimento</label>
                                    <div class="input-group">
                                        <select class="custom-select" name="procedure_value[]" required>
                                            <option value="">Selecione...</option>
                                            <?php if($procedures):
                                            foreach($procedures as $proc):
                                            ?>
                                                <option value="<?=$proc->id;?>|<?=$proc->value;?>"><?=$proc->name;?></option>
                                            <?php endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <label class="floating-label">Dente</label>
                                    <div class="input-group">
                                        <select name="tooth[]" class="custom-select">
                                            <?php
                                                $teeth = [
                                                    11,12,13,14,15,16,17,18,22,23,24,25,26,27,28,
                                                    31,32,33,34,35,36,37,38,41,42,43,44,45,46,47,48
                                                ];
                                            ?>
                                            <option value="99">Selecione</option>
                                            <?php foreach($teeth as $tooth):?>
                                                <option value="<?=$tooth;?>"><?=$tooth;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <div style="font-size:1.5em" class="text-danger cursor-pointer ml-3 del d-none" onClick="del(this)">
                                            <i class="feather icon-trash-2"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="html-paste"></div>
                            <div class="btn btn-sm btn-info mt-3 add">+ procedimentos</div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="text-right">
                            <button class="btn btn-success">Add Procedimentos</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>