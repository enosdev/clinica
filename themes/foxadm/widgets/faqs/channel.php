<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Enquetes</h4>

    <?php if (!$channel): ?>

    <div class="media align-items-center py-3 mb-3">
        <div id="image-holder">
            <img src="<?=theme("/assets/img/noimage.png", CONF_VIEW_ADMIN);?>" alt="" class="d-block ui-w-100 rounded-circle">
        </div>
        <div class="media-body ml-4">
            <h4 class="font-weight-bold mb-0">foto<span class="text-muted font-weight-normal">@capa</span></h4>
        </div>
    </div>

    <div class="card mb-4">
        <h6 class="card-header">Nova Enquete</h6>
        <div class="card-body">
            <form action="<?= url("/".PATH_ADMIN."/faq/channel"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">*Pergunta:</label>
                        <input type="text" name="channel" class="form-control" placeholder="Pergunta" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label">*Data da Publicação:</label>
                        <input type="text" name="created_at" class="form-control mask-datetime datetimepicker" value="<?= date('d/m/Y H:i');?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label">*Data para expirar:</label>
                        <input type="text" name="expire_at" class="form-control mask-datetime datetimepicker" value="<?= date('d/m/Y H:i',strtotime("+23 Days"));?>" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto:</label>
                        <input id="file" type="file" name="cover">
                        <small class="form-text text-muted">Imagem de capa com resolução (1920x1080px).</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label">*Status:</label>
                    <select name="status" class="custom-select" required>
                        <option value="post">Publicar</option>
                        <option value="draft">Rascunho</option>
                        <option value="trash">Lixo</option>
                    </select>
                </div>
                
                <button class="btn btn-success">Criar Pergunta</button>
            </form>
        </div>
    </div>

    <?php else: ?>
    <div class="media align-items-center py-3 mb-3">
        <div id="image-holder">
            <img src="<?= image($channel->cover, 100, 100); ?>" alt="" class="d-block ui-w-100 rounded-circle">
        </div>
        <div class="media-body ml-4">
            <h4 class="font-weight-bold mb-0"><a href="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" class="btn btn-success"><i class="feather icon-plus"></i> Criar resposta</a></h4>
        </div>
    </div>

    <div class="card mb-4">
        <h6 class="card-header">Editar <span class="text-muted font-weight-normal">#<?= $channel->channel; ?></span> </h6>
        <div class="card-body">
            <form action="<?= url("/".PATH_ADMIN."/faq/channel/{$channel->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">*Pergunta:</label>
                        <input type="text" name="channel" class="form-control" value="<?= $channel->channel; ?>" placeholder="Pergunta" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label">*Data da Publicação:</label>
                        <input type="text" name="created_at" class="form-control mask-datetime datetimepicker" value="<?= date_fmt_br($channel->created_at) ;?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label">*Data para expirar:</label>
                        <input type="text" name="expire_at" class="form-control mask-datetime datetimepicker" value="<?= date_fmt_br($channel->expire_at) ;?>" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label w-100">Foto:</label>
                        <input id="file" type="file" name="cover">
                        <small class="form-text text-muted">Imagem de capa com resolução (1920x1080px).</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label">*Status:</label>
                    <?php
                    $sta = $channel->status;
                    $select = function ($value) use ($sta) {
                        return ($sta == $value ? "selected" : "");
                    };
                    ?>
                    <select name="status" class="custom-select" required>
                        <option value="post" <?=$select('post');?>>Publicar</option>
                        <option value="draft" <?=$select('draft');?>>Rascunho</option>
                        <option value="trash" <?=$select('trash');?>>Lixo</option>
                    </select>
                </div>
                
                <button class="btn btn-success">Atualizar</button>
                <a href="#" class="btn btn-dark"
                       data-post="<?= url("/".PATH_ADMIN."/faq/channel/{$channel->id}"); ?>"
                       data-action="delete"
                       data-confirm="Tem certeza que deseja esta pergunta e todas as suas respostas?"
                       data-plan_id="<?= $channel->id; ?>">Excluir Pergunta</a>
            </form>
        </div>
    </div>

    <?php endif; ?>

</div>

<?php $v->start("scripts");?>
    <script src="<?= url("/shared/scripts/jquery.mask.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/datetimepicker.js"); ?>"></script>
    <script>
        $(function(){
            $('.mask-date').mask('00/00/0000');
            $('.mask-doc').mask('000.000.000-00', { reverse: true });
            //date time picker
            
            $.datetimepicker.setLocale('pt');
            $('.datetimepicker').datetimepicker({
            	mask: '39/19/9999 29:59',
            	format: 'd/m/Y H:i'
            });
        });
    </script>
<?php $v->end();?>