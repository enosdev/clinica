<?php $v->layout("_admin");?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Enquetes</h4>

    <div class="media align-items-center py-3 mb-3">
        <div id="image-holder">
            <img src="<?= image($channel->cover, 100, 100); ?>" alt="" class="d-block ui-w-100 rounded-circle">
        </div>
        <div class="media-body ml-4">
            <h4 class="font-weight-bold mb-0"><?=$channel->channel;?></h4>
        </div>
    </div>

    <?php if (!$question): ?>

    <div class="card mb-4">
        <h6 class="card-header">Adicionando opção de resposta</h6>
        <div class="card-body">
            <form action="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">*Resposta:</label>
                        <input type="text" name="question" class="form-control" placeholder="Inserir resposta" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label">*Ordem:</label>
                        <input type="number" name="order_by" class="form-control" value="1" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <button class="btn btn-success">Criar Pergunta</button>
            </form>
        </div>
    </div>

    <?php else: ?>

    <div class="card mb-4">
        <h6 class="card-header"><a href="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" class="btn btn-success"><i class="feather icon-plus"></i> Nova resposta</a> </h6>
        <div class="card-body">
            <form action="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}/{$question->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">*Resposta:</label>
                        <input type="text" name="question" value="<?= $question->question; ?>" class="form-control" placeholder="Inserir resposta" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="form-label">*Ordem:</label>
                        <input type="number" name="order_by" class="form-control" value="<?= $question->order_by; ?>" required>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="form-label">Votos:</label>
                        <input type="number" name="votes" class="form-control" value="<?= $question->votes; ?>" required>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <button class="btn btn-info">Atualizar</button>
                <a href="#" class="btn btn-dark"
                       data-post="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}/{$question->id}"); ?>"
                       data-action="delete"
                       data-confirm="Tem certeza que deseja excluir a perguntas e a respostas?"
                       data-question_id="<?= $question->id; ?>">Excluir Resposta</a>
            </form>
        </div>
    </div>

    <?php endif; ?>

</div>

<?php $v->start("scripts");?>
    <script src="<?= url("/shared/scripts/jquery.mask.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/datetimepicker.js"); ?>"></script>
    <script>
        $(function(){
            $('.mask-date').mask('00/00/0000');
            $('.mask-doc').mask('000.000.000-00', { reverse: true });
            //date time picker
            
            $.datetimepicker.setLocale('pt');
            $('.datetimepicker').datetimepicker({
            	mask: '39/19/9999 29:59',
            	format: 'd/m/Y H:i'
            });
        });
    </script>
<?php $v->end();?>