<?php $v->layout("_admin");?>

<div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-0">Enquetes</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <a href="<?= url("/".PATH_ADMIN."/faq/channel"); ?>" class="btn btn-success"><i class="feather icon-plus"></i> Add enquete</a>
    </div>
    <div class="row">
    <?php if (!$channels): ?>
        <div class="alert alert-info alert-dismissible fade show">
            <i class="fas fa-info"></i> Ainda não existem Enquetes cadastradas.
        </div>
    <?php else: ?>
        <?php foreach ($channels as $channel):?>
        <div class="col-sm-4">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="media align-items-center">
                        <img src="<?= image($channel->cover, 100, 100); ?>" alt="" class="ui-w-60 rounded-circle">
                        <div class="media-body ml-3">
                            <h5 class="mb-2"><?= $channel->channel; ?></h5>
                            <div class="text-muted small"><strong>Publicação:</strong> <?=date_fmt($channel->created_at);?></div>
                            <div class="text-muted small"><strong>Expira em:</strong> <?=date_fmt($channel->expire_at);?></div>
                            <div class="text-muted"><?=($channel->status == "post" ? "<span class='badge badge-pill badge-success'>Público</span>" : ($channel->status == "draft" ? "<span class='badge badge-pill badge-warning'>Rascunho</span>" : "<span class='badge badge-pill badge-danger'>Lixo</span>")); ?> <?=($channel->expire_at < date("Y-m-d H:i:s"))? "<span class='badge badge-pill badge-danger mx-2'><i class='far fa-clock'></i>Expirado</span>" : '';?></div>
                            <div class="mt-3">
                                <a href="<?= url("/".PATH_ADMIN."/faq/channel/{$channel->id}"); ?>" class="btn btn-info"><i class="fas fa-pen"></i> Editar</a>
                                <a href="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" class="btn btn-dark"><i class="fas fa-plus"></i> Resposta</a>
                            </div>
                        </div>
                        <!-- <a href="#" class="btn icon-btn borderless btn-success btn-round d-block px-2"><span class="ion ion-md-mail"></span>teste</a> -->
                    </div>
                </div>
                <div class="card-footer py-3">
                    <?php
                    $channelId = $channel->id;
                    $edit = function ($id) use ($channelId) {
                        $url = url("/".PATH_ADMIN."/faq/question/{$channelId}/{$id}");
                        return "<a href=\"{$url}\" class=\"feather icon-edit-2 btn btn-dark btn-sm\" title=\"Editar pergunta\"></a>";
                    };
                    ?>
                    <?php if (!$channel->questions()->count()): ?>
                        <div class="alert alert-info alert-dismissible fade show">
                            <i class="fas fa-info"></i> Ainda não existem Respostas.
                        </div>
                    <?php else: 
                        $votes = 0;
                        foreach ($channel->questions()->fetch(true) as $question):
                            $votes += $question->votes;?>
                            <p class="mb-2"><?= $edit($question->id); ?> - <?= $question->question; ?> >>> <strong><?=$question->votes;?> votos</strong></p>

                        <?php endforeach; ?>
                        <hr>
                        Total de votos: <?=$votes;?>
                    <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    <?php endif;?>
    </div>

</div>