<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Lista de Agendamentos</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item">Agenda</li>
            <li class="breadcrumb-item active">Lista de agendados</li>
        </ol>
    </div>

    <hr class="border-light container-m--x mt-0 mb-4">

    <!-- liveline-section start -->
    <div class="col-sm-12 d-none">
        <div class="card text-right">
            <div class="card-body text-center">
                <div class="row align-items-center m-l-0">
                    <div class="col-md">
                        <form class="form-group" action="<?= url("/".PATH_ADMIN."/cards/home"); ?>">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar Cliente" name="s" value="<?= $search; ?>">
                                <span class="input-group-append">
                                    <button class="btn btn-primary">Buscar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if(!$list):?>
        <div class="alert alert-info alert-dismissible fade show">
            <i class="fas fa-info"></i> Ainda não existe Prontuário cadastrado.
        </div>
    <?php else:?>
        <div class="row">
            <!-- customar project  start -->
            <?php foreach($list as $item):
                $hoje = date("Y-m-d H:i:s");

                if($item->attendance_at == null || $item->attendance_at == ''){
                    $class = '<span class="badge badge-warning">Aguardando</span>';
    
                    if($hoje > $item->sheduled_at){
                        $class = '<span class="badge badge-danger">Não tendido</span>';
                    }
                } else {
                    $class = '<span class="badge badge-success">Atendido</span>';
                }
            ?>
            <div class="col-sm-6 col-md-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body">
                                <h5 class="mb-3"><?= date_fmt($item->sheduled_at, "d/m/Y \à\s H:i"); ?></h5>
                                <h5 class="mt-2 mb-2"><?=$item->card()->client()->first_name;?> <?=$item->card()->client()->last_name;?></h5>
                                <h5 class="mt-2 mb-2">Marcado para <?=($item->type == 'budget')? '<span class="text-primary font-italic">Orçamento</span>':'<span class="text-success font-italic">Procedimento</span>';?></h5>
                                <div class="text-muted small mb-2"><?=$item->doctor()->first_name;?> <?=$item->doctor()->last_name;?></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center p-0">
                        <div class="row no-gutters row-bordered">
                            <div class="col flex-column pb-2 pt-2">
                                <?=$class;?>
                            </div>
                            <div class="col flex-column pb-2 pt-2">
                                <a href="<?= url("/".PATH_ADMIN."/cards/card/{$item->card}"); ?>" class="btn btn-info btn-sm">Verificar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <!-- customar project  end -->
        </div>        

    <?php endif;?>
    
    <!-- liveline-section end -->
</div>