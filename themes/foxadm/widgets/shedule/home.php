<?php $v->layout("_admin"); 
    $v->start("pages");
?>
<link rel="stylesheet" href="<?=theme("/assets/libs/fullcalendar/fullcalendar.css", CONF_VIEW_ADMIN);?>">
<?php $v->end();?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Calendário</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item">Agenda</li>
            <li class="breadcrumb-item active">Calendário completo</li>
        </ol>
    </div>

    <hr class="border-light container-m--x mt-0 mb-4">

    <!-- Event modal -->
    <form class="modal modal-top fade" id="fullcalendar-default-view-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add an event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">Title</label>
                        <input type="text" class="form-control">
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Type</label>
                        <select class="custom-select">
                            <option value="" selected>Default</option>
                            <option value="fc-event-success">Success</option>
                            <option value="fc-event-info">Info</option>
                            <option value="fc-event-warning">Warning</option>
                            <option value="fc-event-danger">Danger</option>
                            <option value="fc-event-dark">Dark</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default md-btn-flat" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary md-btn-flat">Save</button>
                </div>
            </div>
        </div>
    </form>
    <!-- / Event modal -->
    
    <div class="card mb-4">
        <div class="card-body">
            <div id='fullcalendar-default-view'></div>
        </div>
    </div>

    <!-- List modal -->
    <div class="modal fade" id="view-result-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agendado para <span class="type">{Type}</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">
                    <div class="d-none budget">
                        <span class="doctor">{Doctor}</span> está com <span class="type">{Type}</span> marcado para <span class="day">{Day}</span> com cliente <span class="client">{client}</span>
                    </div>
                    <div class="d-none procedure">
                        <span class="doctor">{Doctor}</span> finalizou <span class="type">{Type}</span> às <span class="day_end">{Day End}</span> com cliente <span class="client">{client}</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default md-btn-flat" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- / List modal -->
    
    <?php 
        //obetendo dados da tabela shedule
    if($shedule):
        foreach($shedule as $item):
            $hoje = date("Y-m-d H:i:s");

            if($item->attendance_at == null || $item->attendance_at == ''){
                $class = 'fc-event-info';

                if($hoje > $item->sheduled_at){
                    $class = 'fc-event-danger';
                }
            } else {
                $class = 'fc-event-success';
            }

            $json[] = [
                "id" => $item->id,
                "title" => $item->doctor()->first_name,
                // "card" => $item->card,
                "start" => $item->sheduled_at,
                "end" => date("Y-m-d H:i:s",strtotime("+40 minutes", strtotime($item->sheduled_at))),
                // "type" => $item->type,
                "className" => $class
            ];
        endforeach;
        //echo json_encode($json);
        $eventList = json_encode($json);
    endif;
    ?>

</div>
<?php $v->start("scripts");?>
    <script src="<?=theme("/assets/libs/moment/moment.js",CONF_VIEW_ADMIN);?>"></script>
    <script src="<?=theme("/assets/libs/fullcalendar/fullcalendar.js",CONF_VIEW_ADMIN);?>"></script>
    <script>
        $(function () {
            var today = new Date();

            var eventList = <?=$eventList;?>;

            // Default view
            // color classes: [ fc-event-success | fc-event-info | fc-event-warning | fc-event-danger | fc-event-dark ]
            $('#fullcalendar-default-view').fullCalendar({
                // Bootstrap styling
                themeSystem: 'bootstrap4',
                bootstrapFontAwesome: {
                    close: ' ion ion-md-close',
                    prev: ' ion ion-ios-arrow-back scaleX--1-rtl',
                    next: ' ion ion-ios-arrow-forward scaleX--1-rtl',
                    prevYear: ' ion ion-ios-arrow-dropleft-circle scaleX--1-rtl',
                    nextYear: ' ion ion-ios-arrow-dropright-circle scaleX--1-rtl'
                },

                header: {
                    left: 'title',
                    center: 'month,agendaWeek,agendaDay',
                    right: 'prev,next today'
                },
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                buttonText: {
                    // prev: "Anterior",
                    // next: "Próximo",
                    today: "Hoje",
                    month: "Mês",
                    week: "Semana",
                    day: "Dia",
                    list: "Lista"
                },
                // timeFormat: 'H(:mm)',
                timeFormat: 'H:mm',
                axisFormat: 'HH:mm',
                // timeFormat: 'HH:mm{ - HH:mm}',
                // minTime: 0,
                // maxTime: 24,
                ignoreTimezone: false,
                weekLabel: "Sm",
                allDayText: "dia inteiro",
                eventLimitText: function (n) {
                    return "mais +" + n;
                },
                noEventsMessage: "Não há eventos para mostrar",

                defaultDate: today,
                navLinks: true, // can click day/week names to navigate views
                selectable: true,
                selectHelper: true,
                weekNumbers: false, // Show week numbers
                nowIndicator: true, // Show "now" indicator
                firstDay: 1, // Set "Monday" as start of a week
                businessHours: {
                    dow: [1, 2, 3, 4, 5, 6], // Monday - Friday
                    start: '8:00',
                    end: '18:00',
                },
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: eventList,
                select: function (start, end) {
                    $('#fullcalendar-default-view-modal').on('shown.bs.modal', function() {
                        $(this).find('input[type="text"]').trigger('focus');
                    }).on('hidden.bs.modal', function() {
                        $(this).off('shown.bs.modal hidden.bs.modal submit').find('input[type="text"], select').val('');
                        $('#fullcalendar-default-view').fullCalendar('unselect');
                    }).on('submit', function(e) {
                        e.preventDefault();
                        var title = $(this).find('input[type="text"]').val();
                        var className = $(this).find('select').val() || null;

                        if (title) {
                            var eventData = {
                                title: title,
                                start: start,
                                end: end,
                                className: className
                            }
                            $('#fullcalendar-default-view').fullCalendar('renderEvent', eventData, true);
                        }

                        $(this).modal('hide');
                    }).modal('show');
                },
                eventClick: function(calEvent, jsEvent, view) {
                    $('#view-result-modal').on('shown.bs.modal',function(){
                        $.post('<?= url("/".PATH_ADMIN."/shedule/home");?>', {shedule: true, card: calEvent.id}, function (response) {
                            if(response.day_end == null){
                                $('.budget').removeClass('d-none')
                                $('.budget span').css('font-weight', 'bold')
                            }else{
                                $('.procedure').removeClass('d-none')
                                $('.procedure span').css('font-weight', 'bold')
                            }

                            $('.doctor').html(response.doctor);
                            $('.type').html(response.type);
                            $('.day').html(response.day);
                            $('.day_end').html(response.day_end);
                            $('.client').html(response.client);

                        }, "json");
                        //alert('teste');
                    }).modal('show');
                }
            });
            
        });

    </script>
<?php $v->end();?>