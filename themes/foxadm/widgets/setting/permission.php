<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Permissões</h4>

    <?php if (!$permission): ?>

        <div class="card mb-4">
            <h6 class="card-header">Adicionar Nova Permissão</h6>
            <div class="card-body">
                <form action="<?= url("/".PATH_ADMIN."/setting/permission"); ?>" method="post">
                    <input type="hidden" name="action" value="create"/>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="form-label">*Título:</label>
                            <input type="text" name="title" class="form-control" placeholder="título" required>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label">*Nome Slug</label>
                            <input type="text" name="uri" class="form-control" placeholder="slug" required>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <button class="btn btn-success">Criar permissão</button>
                </form>
            </div>
        </div>

    <?php else: ?>

        <div class="card mb-4">
            <h6 class="card-header">Adicionar Nova Permissão</h6>
            <div class="card-body">
                <form action="<?= url("/".PATH_ADMIN."/setting/permission/{$permission->id}"); ?>" method="post">
                    <input type="hidden" name="action" value="update"/>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="form-label">*Título:</label>
                            <input type="text" name="title" class="form-control" placeholder="título" value="<?= $permission->title; ?>" required>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label">*Nome Slug</label>
                            <input type="text" name="uri" class="form-control" placeholder="slug" value="<?= $permission->uri; ?>" required>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <button class="btn btn-info">Atualizar</button>
                </form>
            </div>
        </div>

    <?php endif; ?>

</div>