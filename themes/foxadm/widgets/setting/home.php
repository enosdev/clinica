<?php $v->layout("_admin"); ?>

<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Configuração</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-settings"></i></a></li>
            <li class="breadcrumb-item active" >Configurações</li>
        </ol>
    </div>

    <hr class="border-light container-m--x my-4">
    <div class="text-muted small font-weight-bold py-3">Permissões</div>

    <div class="row">
        <?php foreach($permissions as $permit):?>
        <div class="col-sm-3">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="media-body ml-3">
                        <h5 class="mb-2"><?=$permit->title;?></h5>
                        <div class="text-muted small mb-2"><?=$permit->uri;?></div>
                        <a class="btn btn-round btn-success btn-sm" href="<?= url("/".PATH_ADMIN."/setting/permission/{$permit->id}"); ?>" title="editar"><i class="fas fa-pen"></i></a>
                        <a class="btn btn-round btn-danger btn-sm" href="#"
                            data-post="<?= url("/".PATH_ADMIN."/setting/permission/{$permit->id}"); ?>"
                            data-action="delete"
                            data-confirm="ATENÇÃO: Tem certeza que deseja excluir esse Permissão? Essa ação não pode ser desfeita!"
                            data-permission_id="<?= $permit->id; ?>"><i class="fas fa-trash"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>

</div>
