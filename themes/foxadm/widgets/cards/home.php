<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Prontuários</h4>

    <!-- liveline-section start -->
    <div class="col-sm-12">
        <div class="card text-right">
            <div class="card-body text-center">
                <div class="row align-items-center m-l-0">
                    <div class="col-md">
                        <form class="form-group" action="<?= url("/".PATH_ADMIN."/cards/home"); ?>">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar Cliente" name="s" value="<?= $search; ?>">
                                <span class="input-group-append">
                                    <button class="btn btn-primary">Buscar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if(!$cards):?>
        <div class="alert alert-info alert-dismissible fade show">
            <i class="fas fa-info"></i> Ainda não existe Prontuário cadastrado.
        </div>
    <?php else:?>
        <div class="xrow">
            <!-- customar project  start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center m-l-0">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nome</th>
                                        <th>Telefone</th>
                                        <th>Aniversário</th>
                                        <th>Status</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($cards as $cd):?>
                                    <tr>
                                        <td>
                                            <span class="text-muted">ID: <?= $cd->registry; ?></span>
                                        </td>
                                        <td><?= $cd->client()->first_name; ?> <?= $cd->client()->last_name; ?></td>
                                        <td><?= $cd->client()->phone; ?></td>
                                        <td><?= date_fmt($cd->client()->datebirth, "d/m/Y"); ?></td>
                                        <td>
                                            <?php if($cd->status == 'post'): ?>
                                                <span class="badge badge-success">Ativo</span>
                                            <?php elseif ($cd->status == 'draft'): ?>
                                                <span class="badge badge-warning">Andamento</span>
                                            <?php else: ?>
                                                <span class="badge badge-danger">Inativo</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?= url("/".PATH_ADMIN."/cards/card/{$cd->id}"); ?>" class="btn btn-info btn-sm">Gerenciar </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customar project  end -->
        </div>
        
        <div class="row paginacao">
            <?=$paginator;?>
        </div>
        

    <?php endif;?>
    
    <!-- liveline-section end -->
</div>