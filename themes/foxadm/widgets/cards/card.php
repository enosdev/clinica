<?php $v->layout("_admin");
    $v->start("pages");
?>
<link rel="stylesheet" href="<?=theme("/assets/libs/select2/select2.css", CONF_VIEW_ADMIN);?>">
<?php $v->end();?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Card do Cliente</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item">Clientes</li>
            <li class="breadcrumb-item active">Card</li>
        </ol>
    </div>

    <?php if(!$cards): ?>
        reading...
    <?php else: ?>
    <!-- Header -->
    <div class="card mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-md-auto col-sm-12">
                    <img src="<?=image($cards->client()->photo_client,100,100);?>" alt class="d-block ui-w-100 rounded-circle mb-3">
                </div>
                <div class="col">
                    <h4 class="font-weight-bold mb-3"><?=$cards->client()->first_name;?> <?=$cards->client()->last_name;?></h4>
                    <a href="javascript:void(0)" class="d-inline-block text-dark mb-3">
                        <strong>Valor do tratamento:</strong>
                        <?php
                            if($proSheet){
                                $soma = 0;
                                foreach($proSheet as $s){
                                    $soma += $s->value;
                                }
                            }
                        ?>
                        <span class="text-muted">R$ <?=str_price($soma) ?? 0;?></span>
                    </a>
                    <div class="text-muted">
                        <strong>Plano de pagamento / OBS:</strong> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Alias officiis nemo fugiat nesciunt tenetur beatae aperiam deleniti, nam nisi error quis, provident eos quasi doloremque explicabo quaerat incidunt non animi.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header -->

    <!-- DataTable within card -->

    <div class="card">
        <?php if ($cards->status == 'draft'):?>
            <?php if(!isset( $cards->sheduleCard()->type ) || $cards->sheduleCard()->type != "budget"):?>
                <div class="card-header py-3 text-center">
                    <div class="alert alert-info alert-dismissible fade show">
                        <i class="fas fa-info"></i> Ainda não existe procedimentos para <strong><?=$cards->client()->first_name;?></strong>.
                    </div>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-report">Enviar para Orçamento</button>
                    <!-- <button class="btn btn-success btn-sm mb-3 btn-round" ><i class="feather icon-plus"></i> Add Doctor</button> -->
                </div>
            <?php else: ?>
                <div class="card-header pb-1 text-center pb-3">
                    <div class="alert alert-dark-warning alert-dismissible fade show">
                        <i class="fas fa-info"></i> Orçamento agendado para <strong><?= date_fmt($cards->sheduleCard()->sheduled_at, "d/m/Y \á\s H:i"); ?></strong>.<br />
                        Aguardando <strong><?=$cards->sheduleCard()->doctor()->first_name;?> <?=$cards->sheduleCard()->doctor()->last_name;?></strong> atender
                    </div>
                    <?php if(user()->level == 5 || user()->level == 6 || user()->level == 8):?>
                        <form action="<?= url("/".PATH_ADMIN."/cards/card/{$cards->id}"); ?>" method="post">
                            <input type="hidden" name="action" value="update_atendimento">
                            <input type="hidden" name="status" value="post">
                            <input type="hidden" name="shedule_id" value="<?=$cards->sheduleCard()->id;?>">
                            <button class="btn btn-primary btn-lg">Iniciar atendimento</button>
                        </form>
                    <?php endif;?>
                </div>
            <?php endif; ?>
        <?php else:?>
        <div class="card-header py-3">
            <strong>Procedimentos</strong>
            <span class="text-muted small">
                Orçamento realizado por <strong>#<?=$cards->sheduleCard()->doctor()->first_name;?> <?=$cards->sheduleCard()->doctor()->last_name;?></strong>
            </span>
        </div>
        <div class="text-right m-2">
        <?php if(user()->level == 5 || user()->level == 6 || user()->level == 8):?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-procedure">Add Procedimentos</button>
        <?php endif;?>
        </div>
        <div class="card-datatable table-responsive px-2 py-2">
            <table class="datatables-demo table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Data Agendamento</th>
                        <th>Tratamentos</th>
                        <th>Dr.</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!$proSheet):?>
                    <div class="alert alert-info alert-dismissible fade show">
                        <i class="fas fa-info"></i> Ainda não existe procedimentos para <strong><?=$cards->client()->first_name;?></strong>.
                    </div>
                    <?php else:
                        foreach($proSheet as $items):
                        $array_tooth[] = $items->tooth;
                        $proce_final[$items->tooth] = $items->status;
                        ?>
                        <tr>
                            <td>
                            <?php if(!$items->sheduleCardProcedure()):?>
                                <?php if(user()->level == 5 || user()->level == 6 || user()->level == 7):?>
                                    <button class="btn btn-sm btn-info btn-open" data-id="<?=$items->id;?>" data-toggle="modal" data-target="#modal-agendar">Agendar</button>
                                <?php else:?>
                                    ??
                                <?php endif;?>
                            <?php else: ?>
                                <form action="<?= url("/".PATH_ADMIN."/cards/card/{$cards->id}"); ?>" method="post">
                                    <input type="hidden" name="action" value="start_atendimento">
                                    <input type="hidden" name="shedule_id" value="<?=$items->sheduleCardProcedure()->id;?>">  
                                    <?php if(!$items->sheduleCardProcedure()->attendance_at):?>
                                        <?=date_fmt($items->sheduleCardProcedure()->sheduled_at);?> <button class="btn btn-sm btn-primary ml-2">Atender</button>
                                    <?php endif;?>
                                </form>

                                <form action="<?= url("/".PATH_ADMIN."/cards/card/{$cards->id}"); ?>" method="post">
                                    <input type="hidden" name="action" value="stop_atendimento">
                                    <input type="hidden" name="shedule_id" value="<?=$items->sheduleCardProcedure()->id;?>">
                                    <input type="hidden" name="procedures_sheet_id" value="<?=$items->sheduleCardProcedure()->procedure_sheet;?>">
                                    <?php if($items->sheduleCardProcedure()->attendance_at && !$items->sheduleCardProcedure()->completed_service):?>
                                        <?=date_fmt($items->sheduleCardProcedure()->sheduled_at);?>
                                        <div style="position:fixed; top:0; left:0; z-index:9999; display:flex; justify-content:center; align-items:center; width:100vw; height:100vh;background:rgba(0,0,0,.95)">
                                            <button class="btn btn-lg btn-success ml-2">Finalizar</button>
                                        </div>
                                    <?php endif;?>
                                </form>

                                <?php if($items->sheduleCardProcedure()->attendance_at && $items->sheduleCardProcedure()->completed_service):?>
                                    <?=date_fmt($items->sheduleCardProcedure()->sheduled_at);?>
                                    <?php
                                        $datatime1 = new DateTime($items->sheduleCardProcedure()->sheduled_at);
                                        $datatime2 = new DateTime($items->sheduleCardProcedure()->completed_service);
                                         
                                        $data1  = $datatime1->format('Y-m-d H:i:s');
                                        $data2  = $datatime2->format('Y-m-d H:i:s');
                                        
                                        $diff = $datatime1->diff($datatime2);
                                        $hora = $diff->h;
                                        $minutos = $diff->i;
                                        $segundos = $diff->s;
                                        echo "<strong>em {$hora}h : {$minutos}min : {$segundos}seg</strong>";
                                    ?>
                                <?php endif;?>
                               
                            <?php endif;?>
                            </td>
                            <td><?=$items->procedure()->name;?>
                                <?php
                                    echo $items->tooth == NULL ? '' : " | <strong><span class=\"text-info\">(Dente = {$items->tooth})</span></strong>";
                                ?>
                            </td>
                            <td><?=$items->doctor()->first_name ?? '??';?></td>
                            <td>
                                <?=($items->status == 'post')? '<span class="badge badge-success">Concluído</span>' : '<span class="badge badge-warning">Pendente</span>' ;?>    
                            </td>
                        </tr>
                    <?php endforeach;
                    endif;?>
                </tbody>
            </table>
        </div>
        <?php endif;?>
    </div>

    <!-- Odontograma -->
    <div class="card text-center">
        <div class="col-md-12 pl-5 pr-5">
            <div class="row no-gutters dentes mb-3">
                <?php
                    $tooth_procedure = array_filter($array_tooth);
                    $teeth = [18,17,16,15,14,13,12,11,21,22,23,24,25,26,27,28];
                    foreach($teeth as $tooth):
                        $pasta = $proce_final[$tooth] ?? '';
                        $path = $pasta == 'post' ? 'finalizado/' : 'andamento/';
                        $tem_tooth = (in_array($tooth, $tooth_procedure))? $path : '';
                    ?>
                            <div class="col flex-column px-2">
                            <img src="<?=theme("/assets/odontograma/{$tem_tooth}{$tooth}.png",CONF_VIEW_ADMIN);?>" alt="">
                        </div>
                <?php endforeach;?>
            </div>
            <div class="row no-gutters dentes mt-3">
                <?php
                    $teeth2 = [48,47,46,45,44,43,42,41,31,32,33,34,35,36,37,38];
                    foreach($teeth2 as $tooth):
                        $pasta = $proce_final[$tooth] ?? '';
                        $path = $pasta == 'post' ? 'finalizado/' : 'andamento/';
                        $tem_tooth = (in_array($tooth, $tooth_procedure))? $path : '';
                    ?>
                            <div class="col flex-column px-2">
                            <img src="<?=theme("/assets/odontograma/{$tem_tooth}{$tooth}.png",CONF_VIEW_ADMIN);?>" alt="">
                        </div>
                <?php endforeach;?>
            </div>
        </div> 
    </div>

    <!-- Financeiro -->
    <div class="card">
        <div class="card-body p-5">
            <div class="row">
                <div class="col-sm-6 text-left pb-4">
                    <h6 class="text-big text-large font-weight-bold mb-3">Ficha #<?=$cards->registry;?></h6>
                    <div class="mb-1">Data início do tratamento:
                        <strong class="font-weight-semibold"><?=$cards->start_treatment? date_fmt($cards->start_treatment,"d/m/Y") : 'Procedimentos ainda não iniciados'; ?></strong>
                    </div>
                </div>
            </div>

            <div class="table-responsive mb-4">
                <table class="table m-0">
                    <thead>
                        <tr>
                            <th class="py-3">
                                Procedimento
                            </th>
                            <th class="py-3">
                                Taxa
                            </th>
                            <th class="py-3">
                                Desconto
                            </th>
                            <th class="py-3">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(!$proSheet):?>
                    <div class="alert alert-info alert-dismissible fade show">
                        <i class="fas fa-info"></i> Ainda não existe procedimentos para <strong><?=$cards->client()->first_name;?></strong>.
                    </div>
                    <?php else:
                        $subtotal = 0;
                        foreach($proSheet as $items):
                            $subtotal += $items->value;
                        ?>
                        <tr>
                            <td class="py-3">
                                <div class="font-weight-semibold"><?=$items->procedure()->name;?></div>
                                <div class="text-muted"><?=$items->note ? $items->note : '<small>...</small>';?></div>
                            </td>
                            <td class="py-3">
                                <strong>R$ <?=str_price($items->value);?></strong>
                            </td>
                            <td class="py-3">
                                <strong>0%</strong>
                            </td>
                            <td class="py-3">
                                <strong>R$ <?=str_price($items->value);?></strong>
                            </td>
                        </tr>
                        <?php endforeach; 
                    endif;?>
                        <tr>
                            <td colspan="3" class="text-right py-3">
                                Subtotal:
                                <!-- <br> Tax(25%): -->
                                <br>
                                <span class="d-block text-big mt-2">Total:</span>
                            </td>
                            <td class="py-3">
                                <strong>R$ <?=str_price($subtotal);?></strong>
                                <!-- <br> <strong>$1,219.05</strong> -->
                                <br>
                                <strong class="d-block text-big mt-2">R$ <?=str_price($subtotal);?></strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-muted">
                <strong>Nota:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet, dolor id dapibus dapibus, neque mi tincidunt quam, quis congue ligula risus vitae magna. Curabitur ultrices nisi massa,
                nec viverra lorem feugiat sed.
                Mauris non porttitor nunc. Integer eu orci in magna auctor vestibulum.
            </div>
        </div>
        <div class="card-footer text-right d-none">
            <a href="#" target="_blank" class="btn btn-default"><i class="ion ion-md-print"></i>&nbsp; Imprimir</a>
            <button type="button" class="btn btn-primary ml-2"><i class="ion ion-ios-paper-plane"></i>&nbsp; Enviar</button>
        </div>
    </div>

    <?php require(__DIR__.'/../modal/modal.php');?>
    
    <?php endif; ?>
</div>

<?php $v->start("scripts");?>
<script src="<?=url("/shared/scripts/datetimepicker.js");?>"></script>
<script src="<?=theme("/assets/libs/select2/select2.js",CONF_VIEW_ADMIN);?>"></script>

<script>
    $(function(){
        
        //montamos um array de 10 em 10 minutos
        let minutos = [];
        var horaEntrada = 8;
        var horaSaida = 18;
        var interval = 10;
        for (horaEntrada; horaEntrada <= horaSaida ; horaEntrada++) {
            minutos.push(`${horaEntrada}`);
            if(horaEntrada < horaSaida){
                for(let i = 1; i <= 50; i++){
                    if(i % interval ===0){
                        minutos.push(`${horaEntrada}:${i}`);
                    }
                }              
            }
        }

        //inserir valor de qual procedimento está sendo selecionado
        //para agendar
        $('.btn-open').on('click',function(){
            var value = $(this).attr('data-id');
            $("input[data-sp='shedule-procedure']").val(value);
        });

        //date time picker
        $.datetimepicker.setLocale('pt');
        $('.datetimepicker').datetimepicker({
            // theme:'dark',
            allowTimes: minutos,
        	mask: '39/19/9999 29:59',
        	format: 'd/m/Y H:i'
        });

        //mostra lista agenda para o dentista selecionado
        $('select[name="user"]').change(function(){
            var userDoctor = $(this).val();
            $.post('<?= url("/".PATH_ADMIN."/cards/card");?>', {refresh: true, user: userDoctor}, function (response) {
                //list
                var list = "";
                if (response.list) {
                    $.each(response.list, function (item, data) {
                        // var url = '<?= url();?>' + data.url;
                        // var title = '<?= strtolower(CONF_SITE_NAME);?>';

                        list += "<div class=\"text-primary text-big\">";
                        list += data.sheduled_at;
                        list += "<hr>";
                        list += "</div>";
                    });
                } else {
                    list = "<div class=\"alert alert-info alert-dismissible fade show\">" +
                                "<i class=\"fas fa-info\"></i> Não existem agendamentos para seleção feita." +
                            "</div>";
                }

                $(".result").html(list);
                console.log(response)
            }, "json");

            return false;


        });

        //multiselect dos procedimentos
        $('.select-procedures').each(function() {
            $(this)
            .wrap('<div class="position-relative"></div>')
            .select2({
                placeholder: 'Selecione um procedimento',
                dropdownParent: $(this).parent()
            });
        })

        //adiciona nova linha de procedimentos
        $('.add').on('click', function(){
            let html = $('.html-copy').html();
            $('.html-paste').append('<div class="row mt-2" style="border-top: solid 1px #eee">'+html+'</div>');
            $('.html-paste .del').removeClass('d-none');
        });

        // var odonto = $("input[name='odontograma']:checked");
        var odonto = $("input[name='odontograma']");
        odonto.on('click',function(){
            var valor = $("input[name='odontograma']:checked").val();
            if (valor == 'yes'){
                $('.odontograma').removeClass('d-none');
            } else {
                $('.odontograma').addClass('d-none');
            }
        });


        // $('.html-paste .row .del').on('click',function(){
        //     console.log('clicl');
        //     // $(this).parent().parent().parent().addClass('d-none');
        // });

        //inicia atendimento
        // $("#inicia_atendimento").on('click',function(){
        //     if(confirm("Deseja realmente iniciar o atendimento?") == true){
        //         window.location.href = '<?=url(PATH_ADMIN.'/');?>'
        //     }
        // })

    });

    // "use strict";
    function del(e){
        let apagando = e.parentElement.parentElement.parentElement
        apagando.parentNode.removeChild(apagando)
    }

</script>

<?php $v->end();?>