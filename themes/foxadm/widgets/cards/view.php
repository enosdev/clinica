<?php $v->layout("_admin"); ?>
<div class="container-fluid flex-grow-1 container-p-y">
    <h4 class="font-weight-bold py-3 mb-0">Ficha do Cliente</h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item">Clientes</li>
            <li class="breadcrumb-item active">Card</li>
        </ol>
    </div>

    <!-- Header -->
    <div class="card mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-md-auto col-sm-12">
                    <img src="http://localhost/admin/themeforest-8RhrDnEl-the-empire-bootstrap-4-admin-template/Template/assets/img/avatars/5.png" alt class="d-block ui-w-100 rounded-circle mb-3">
                </div>
                <div class="col">
                    <h4 class="font-weight-bold mb-3">Nellie Maxwell</h4>
                    <a href="javascript:void(0)" class="d-inline-block text-dark mb-3">
                        <strong>Valor do tratamento:</strong>
                        <span class="text-muted">R$ 800,00</span>
                    </a>
                    <div class="text-muted">
                        <strong>Plano de pagamento / OBS:</strong> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Alias officiis nemo fugiat nesciunt tenetur beatae aperiam deleniti, nam nisi error quis, provident eos quasi doloremque explicabo quaerat incidunt non animi.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header -->

    <!-- DataTable within card -->

    <div class="card">
        <div class="card-header py-3">
            <strong>Orçamento</strong>
            <span class="text-muted small">
                #Felipe Moreira
            </span>
        </div>
        <div class="card-datatable table-responsive px-2 py-2">
            <table class="datatables-demo table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Data Agendamento</th>
                        <th>Tratamentos</th>
                        <th>Dr.</th>
                        <th>Débito</th>
                        <th>Crédito</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>22/05/2019</td>
                        <td>Bloco de Porcelana Pura</td>
                        <td>??</td>
                        <td>x</td>
                        <td>X</td>
                        <td><span class="badge badge-success">Realizado</span></td>
                    </tr>
                    <tr>
                        <td>22/08/2019</td>
                        <td>Bichectomia</td>
                        <td>??</td>
                        <td>x</td>
                        <td>X</td>
                        <td><span class="badge badge-danger">Pendente</span></td>
                    </tr>
                    <tr>
                        <td>22/08/2020</td>
                        <td>Bichectomia</td>
                        <td>??</td>
                        <td>x</td>
                        <td>X</td>
                        <td><span class="badge badge-warning">Andamento</span></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>

    <div class="card text-center">
        <div class="col-md-12">
            <img src="<?=theme("/assets/img/odontograma.png",CONF_VIEW_ADMIN);?>" alt="" class="img-fluid">
        </div> 
    </div>

    <div class="card">
        <div class="card-body p-5">
            <div class="row">
                <div class="col-sm-6 text-left pb-4">
                    <h6 class="text-big text-large font-weight-bold mb-3">Ficha #49029</h6>
                    <div class="mb-1">Data cadastro:
                        <strong class="font-weight-semibold">12 Janeiro 2020</strong>
                    </div>
                    <div>Última atualização:
                        <strong class="font-weight-semibold">15 Janeiro 2020</strong>
                    </div>
                </div>
            </div>

            <div class="table-responsive mb-4">
                <table class="table m-0">
                    <thead>
                        <tr>
                            <th class="py-3">
                                Procedimento
                            </th>
                            <th class="py-3">
                                Taxa
                            </th>
                            <th class="py-3">
                                Quantidade
                            </th>
                            <th class="py-3">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="py-3">
                                <div class="font-weight-semibold">Precedimento efetuado e pago</div>
                                <div class="text-muted">pequena descrição caso precise.</div>
                            </td>
                            <td class="py-3">
                                <strong>R$ 50,00</strong>
                            </td>
                            <td class="py-3">
                                <strong>2</strong>
                            </td>
                            <td class="py-3">
                                <strong>R$ 100,00</strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="py-3">
                                <div class="font-weight-semibold">Outro procedimento</div>
                                <div class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
                            </td>
                            <td class="py-3">
                                <strong>R$ 47,95</strong>
                            </td>
                            <td class="py-3">
                                <strong>36</strong>
                            </td>
                            <td class="py-3">
                                <strong>$1.726,20</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-right py-3">
                                Subtotal:
                                <br> Tax(25%):
                                <br>
                                <span class="d-block text-big mt-2">Total:</span>
                            </td>
                            <td class="py-3">
                                <strong>$4,876.20</strong>
                                <br>
                                <strong>$1,219.05</strong>
                                <br>
                                <strong class="d-block text-big mt-2">$6,095.25</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-muted">
                <strong>Nota:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet, dolor id dapibus dapibus, neque mi tincidunt quam, quis congue ligula risus vitae magna. Curabitur ultrices nisi massa,
                nec viverra lorem feugiat sed.
                Mauris non porttitor nunc. Integer eu orci in magna auctor vestibulum.
            </div>
        </div>
        <div class="card-footer text-right">
            <a href="pages_invoice-print.html" target="_blank" class="btn btn-default"><i class="ion ion-md-print"></i>&nbsp; Print</a>
            <button type="button" class="btn btn-primary ml-2"><i class="ion ion-ios-paper-plane"></i>&nbsp; Send</button>
        </div>
    </div>


    <!-- Side info -->
    <div class="card mb-4">
        <div class="card-header py-3">
            <strong>Financeiro</strong>
        </div>
        <div class="card-body">
            <p class="mb-2">
                <i class="ion ion-md-desktop ui-w-30 text-center text-lighter"></i> UI/UX Designer</p>
            <p class="mb-2">
                <i class="ion ion-ios-navigate ui-w-30 text-center text-lighter"></i> London, United Kingdom</p>
            <p class="mb-0">
                <i class="ion ion-md-globe ui-w-30 text-center text-lighter"></i>
                <a href="javascript:void(0)" class="text-dark">website.com</a>
            </p>
        </div>
        <hr class="border-light m-0">
        <div class="card-body">
            <a href="javascript:void(0)" class="d-block text-dark mb-2">
                <i class="ion ion-logo-twitter ui-w-30 text-center text-twitter"></i> @nmaxwell
            </a>
            <a href="javascript:void(0)" class="d-block text-dark mb-2">
                <i class="ion ion-logo-facebook ui-w-30 text-center text-facebook"></i> nmaxwell
            </a>
            <a href="javascript:void(0)" class="d-block text-dark mb-0">
                <i class="ion ion-logo-instagram ui-w-30 text-center text-instagram"></i> nmaxwell
            </a>
        </div>
    </div>
    <!-- / Side info -->


    <div class="row">
        <div class="col">

            <!-- Info -->
            <div class="card mb-4">
                <div class="card-header py-3">
                    <strong>Financeiro</strong>
                </div>
                <div class="card-body">

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Valor Pago:</div>
                        <div class="col-md-9">
                            R$ 300,00
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h6>With Icon</h6>
                        <hr>
                        <div id="accordion2">
                            <div class="card mb-2">
                                <div class="card-header">
                                    <a class="d-flex justify-content-between text-dark" data-toggle="collapse" aria-expanded="true" href="#accordion2-1">Personal details<div class="collapse-icon"></div></a>
                                </div>

                                <div id="accordion2-1" class="collapse" data-parent="#accordion2">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                        Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                                        occaecat craft beer farm-to-table,
                                        raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-2">
                                <div class="card-header">
                                    <a class="collapsed d-flex justify-content-between text-dark" data-toggle="collapse" href="#accordion2-2">Education details<div class="collapse-icon"></div></a>
                                </div>
                                <div id="accordion2-2" class="collapse" data-parent="#accordion2">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                        Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                                        occaecat craft beer farm-to-table,
                                        raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-2">
                                <div class="card-header">
                                    <a class="collapsed d-flex justify-content-between text-dark" data-toggle="collapse" href="#accordion2-3">Department Details<div class="collapse-icon"></div></a>
                                </div>
                                <div id="accordion2-3" class="collapse" data-parent="#accordion2">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                        Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                                        occaecat craft beer farm-to-table,
                                        raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Country:</div>
                        <div class="col-md-9">
                            <a href="javascript:void(0)" class="text-dark">Canada</a>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Languages:</div>
                        <div class="col-md-9">
                            <a href="javascript:void(0)" class="text-dark">English</a>
                        </div>
                    </div>

                    <h6 class="my-3">Contacts</h6>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Phone:</div>
                        <div class="col-md-9">
                            +0 (123) 456 7891
                        </div>
                    </div>

                    <h6 class="my-3">Interests</h6>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Favorite music:</div>
                        <div class="col-md-9">
                            <a href="javascript:void(0)" class="text-dark">Rock</a>,
                            <a href="javascript:void(0)" class="text-dark">Alternative</a>,
                            <a href="javascript:void(0)" class="text-dark">Electro</a>,
                            <a href="javascript:void(0)" class="text-dark">Drum &amp; Bass</a>,
                            <a href="javascript:void(0)" class="text-dark">Dance</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 text-muted">Favorite movies:</div>
                        <div class="col-md-9">
                            <a href="javascript:void(0)" class="text-dark">The Green Mile</a>,
                            <a href="javascript:void(0)" class="text-dark">Pulp Fiction</a>,
                            <a href="javascript:void(0)" class="text-dark">Back to the Future</a>,
                            <a href="javascript:void(0)" class="text-dark">WALL·E</a>,
                            <a href="javascript:void(0)" class="text-dark">Django Unchained</a>,
                            <a href="javascript:void(0)" class="text-dark">The Truman Show</a>,
                            <a href="javascript:void(0)" class="text-dark">Home Alone</a>,
                            <a href="javascript:void(0)" class="text-dark">Seven Pounds</a>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center p-0">
                    <div class="row no-gutters row-bordered row-border-light">
                        <a href="javascript:void(0)" class="d-flex col flex-column text-dark py-3">
                            <div class="font-weight-bold">24</div>
                            <div class="text-muted small">posts</div>
                        </a>
                        <a href="javascript:void(0)" class="d-flex col flex-column text-dark py-3">
                            <div class="font-weight-bold">51</div>
                            <div class="text-muted small">videos</div>
                        </a>
                        <a href="javascript:void(0)" class="d-flex col flex-column text-dark py-3">
                            <div class="font-weight-bold">215</div>
                            <div class="text-muted small">photos</div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- / Info -->

            <!-- Posts -->

            <div class="card mb-4">
                <div class="card-body">
                    <p>
                        Aliquam varius euismod lectus, vel consectetur nibh tincidunt vitae. In non dignissim est. Sed eu ligula metus. Vivamus eget quam sit amet risus venenatis laoreet ut vel magna. Sed dui ligula, tincidunt in nunc eu, rhoncus iaculis nisi.
                    </p>
                    <p>
                        Sed et convallis odio, vel laoreet tellus. Vivamus a leo eu metus porta pulvinar. Pellentesque tristique varius rutrum.
                    </p>
                    <div class="ui-bordered">
                        <a href="javascript:void(0)" class="ui-rect ui-bg-cover text-white" style="background-image: url('assets/img/bg/1.jpg');">
                            <div class="d-flex justify-content-start align-items-end ui-rect-content p-2">
                                <div class="bg-dark rounded text-white small py-1 px-2">
                                    <i class="ion ion-md-link"></i> &nbsp; external.com/some/page
                                </div>
                            </div>
                        </a>
                        <div class="p-4">
                            <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5> Duis ut quam nec mi bibendum finibus et id tortor. Maecenas tristique dolor enim, sed tristique sem cursus et. Etiam tempus iaculis blandit. Vivamus a justo a elit bibendum pulvinar ut non erat. Cras in purus sed leo mattis consequat viverra id arcu.
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="javascript:void(0)" class="d-inline-block text-muted">
                        <small class="align-middle"><strong>123</strong> Likes</small>
                    </a>
                    <a href="javascript:void(0)" class="d-inline-block text-muted ml-3">
                        <small class="align-middle"><strong>12</strong> Comments</small>
                    </a>
                    <a href="javascript:void(0)" class="d-inline-block text-muted ml-3">
                        <i class="ion ion-md-share align-middle"></i>&nbsp;
                        <small class="align-middle">Repost</small>
                    </a>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus commodo bibendum. Vivamus laoreet blandit odio, vel finibus quam dictum ut.
                    </p>
                    <a href="javascript:void(0)" class="ui-rect ui-bg-cover" style="background-image: url('assets/img/bg/6.jpg');"></a>
                </div>
                <div class="card-footer">
                    <a href="javascript:void(0)" class="d-inline-block text-muted">
                        <small class="align-middle"><strong>123</strong> Likes</small>
                    </a>
                    <a href="javascript:void(0)" class="d-inline-block text-muted ml-3">
                        <small class="align-middle"><strong>12</strong> Comments</small>
                    </a>
                    <a href="javascript:void(0)" class="d-inline-block text-muted ml-3">
                        <i class="ion ion-md-share align-middle"></i>&nbsp;
                        <small class="align-middle">Repost</small>
                    </a>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <p>
                        Aliquam viverra ornare tincidunt. Vestibulum sit amet vestibulum quam. Donec eu est non velit rhoncus interdum eget vel lorem.
                    </p>

                    <div class="border-top-0 border-right-0 border-bottom-0 ui-bordered pl-3 mt-4 mb-2">
                        <div class="media mb-3">
                            <img src="assets/img/avatars/4-small.png" class="d-block ui-w-40 rounded-circle" alt>
                            <div class="media-body ml-3">
                                Kenneth Frazier
                                <div class="text-muted small">3 days ago</div>
                            </div>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus commodo bibendum. Vivamus laoreet blandit odio, vel finibus quam dictum ut.
                        </p>
                        <a href="javascript:void(0)" class="ui-rect ui-bg-cover" style="background-image: url('assets/img/bg/8.jpg');"></a>
                    </div>
                    <a href="javascript:void(0)" class="text-muted small">Reposted from @kfrazier/posts/123</a>
                </div>
                <div class="card-footer">
                    <a href="javascript:void(0)" class="d-inline-block text-muted">
                        <small class="align-middle"><strong>123</strong> Likes</small>
                    </a>
                    <a href="javascript:void(0)" class="d-inline-block text-muted ml-3">
                        <small class="align-middle"><strong>12</strong> Comments</small>
                    </a>
                    <a href="javascript:void(0)" class="d-inline-block text-muted ml-3">
                        <i class="ion ion-md-share align-middle"></i>&nbsp;
                        <small class="align-middle">Repost</small>
                    </a>
                </div>
            </div>

            <!-- / Posts -->

        </div>
        <div class="col-xl-4">

            <!-- Side info -->
            <div class="card mb-4">
                <div class="card-body">
                    <a href="javascript:void(0)" class="btn btn-primary btn-round">+&nbsp; Follow</a> &nbsp;
                    <a href="javascript:void(0)" class="btn icon-btn btn-default md-btn-flat btn-round">
                        <span class="ion ion-md-mail"></span>
                    </a>
                </div>
                <hr class="border-light m-0">
                <div class="card-body">
                    <p class="mb-2">
                        <i class="ion ion-md-desktop ui-w-30 text-center text-lighter"></i> UI/UX Designer</p>
                    <p class="mb-2">
                        <i class="ion ion-ios-navigate ui-w-30 text-center text-lighter"></i> London, United Kingdom</p>
                    <p class="mb-0">
                        <i class="ion ion-md-globe ui-w-30 text-center text-lighter"></i>
                        <a href="javascript:void(0)" class="text-dark">website.com</a>
                    </p>
                </div>
                <hr class="border-light m-0">
                <div class="card-body">
                    <a href="javascript:void(0)" class="d-block text-dark mb-2">
                        <i class="ion ion-logo-twitter ui-w-30 text-center text-twitter"></i> @nmaxwell
                    </a>
                    <a href="javascript:void(0)" class="d-block text-dark mb-2">
                        <i class="ion ion-logo-facebook ui-w-30 text-center text-facebook"></i> nmaxwell
                    </a>
                    <a href="javascript:void(0)" class="d-block text-dark mb-0">
                        <i class="ion ion-logo-instagram ui-w-30 text-center text-instagram"></i> nmaxwell
                    </a>
                </div>
            </div>
            <!-- / Side info -->

            <!-- Skills -->
            <div class="card mb-4">
                <div class="card-header">Skills</div>
                <div class="card-body">

                    <div class="mb-1">HTML -
                        <small class="text-muted">80%</small>
                    </div>
                    <div class="progress mb-3" style="height: 4px;">
                        <div class="progress-bar bg-secondary" style="width: 80%;"></div>
                    </div>

                    <div class="mb-1">CSS -
                        <small class="text-muted">95%</small>
                    </div>
                    <div class="progress mb-3" style="height: 4px;">
                        <div class="progress-bar bg-success" style="width: 95%;"></div>
                    </div>

                    <div class="mb-1">Javascript -
                        <small class="text-muted">90%</small>
                    </div>
                    <div class="progress mb-3" style="height: 4px;">
                        <div class="progress-bar bg-warning" style="width: 90%;"></div>
                    </div>

                    <div class="mb-1">UI/UX -
                        <small class="text-muted">80%</small>
                    </div>
                    <div class="progress" style="height: 4px;">
                        <div class="progress-bar bg-danger" style="width: 80%;"></div>
                    </div>

                </div>
                <a href="javascript:void(0)" class="card-footer d-block text-center text-dark small font-weight-semibold">SHOW ALL SKILLS</a>
            </div>
            <!-- / Skills -->

            <!-- Friends -->
            <div class="card mb-4">
                <div class="card-header with-elements">
                    <span class="card-header-title">Friends &nbsp;<small class="text-muted">591</small></span>
                    <div class="card-header-elements ml-md-auto">
                        <a href="javascript:void(0)" class="btn btn-default md-btn-flat btn-xs">Show All</a>
                    </div>
                </div>
                <div class="row no-gutters row-bordered row-border-light">
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/2-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Leon Wilson</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/3-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Allie Rodriguez</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/4-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Kenneth Frazier</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/5-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Nellie Maxwell</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/6-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Mae Gibson</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/7-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Alice Hampton</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/11-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Belle Ross</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/12-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Arthur Duncan</div>
                    </a>
                    <a href="javascript:void(0)" class="d-flex col-4 col-sm-3 col-md-4 flex-column align-items-center text-dark py-3 px-2">
                        <img src="assets/img/avatars/9-small.png" alt class="d-block ui-w-40 rounded-circle mb-2">
                        <div class="text-center small">Amanda Warner</div>
                    </a>
                </div>
            </div>
            <!-- / Friends -->

            <!-- Photos -->
            <div class="card mb-4">
                <div class="card-header with-elements">
                    <span class="card-header-title">Photos</span>
                    <div class="card-header-elements ml-2">
                        <small class="text-muted">54</small>
                    </div>
                    <div class="card-header-elements ml-md-auto">
                        <a href="javascript:void(0)" class="btn btn-default md-btn-flat btn-xs">Show More</a>
                    </div>
                </div>
                <div class="row no-gutters align-items-start pt-1 pl-1">

                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/1.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/2.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/3.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/4.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/5.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/6.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/7.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/8.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/9.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/10.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/11.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/12.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/13.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/14.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/15.jpg');"></span>
                    </a>
                    <a href="javascript:void(0)" class="d-block col-3 col-sm-2 col-lg-3 pr-1 pb-1">
                        <span class="d-block ui-square ui-bg-cover" style="background-image: url('assets/img/bg/16.jpg');"></span>
                    </a>

                </div>
            </div>
            <!-- / Photos -->

        </div>
    </div>

</div>