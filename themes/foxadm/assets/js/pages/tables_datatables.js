$(function () {
  $('.datatables-demo').dataTable({
    responsive: true,
    paging: false,
    ordering: false,
    info: false,
    language: {
      search: "Procurar:",
      lengthMenu: "Exibir _MENU_ registros por página",
      zeroRecords: "Nada encontrado - desculpe",
      info: "Mostrando página _PAGE_ de _PAGES_ no total de _MAX_ registros",
      infoEmpty: "Nenhum registro disponível",
      infoFiltered: "(filtered from _MAX_ total records)",
      paginate: {
        "first": "Primeiro",
        "last": "Último",
        "next": "Próximo",
        "previous": "Anterior"
      }
    }
  });
});