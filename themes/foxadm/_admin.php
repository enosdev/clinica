<!DOCTYPE html>
<html lang="pt-br" class="default-style layout-fixed layout-navbar-fixed">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    
    <?= $head; ?>

    <meta name="author" content="Enos.Fox" />
    <link rel="icon" type="image/x-icon" href="<?= theme("/assets/img/favicon.ico", CONF_VIEW_ADMIN); ?>">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?= theme("/assets/fonts/fontawesome.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/fonts/ionicons.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/fonts/linearicons.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/fonts/open-iconic.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/fonts/pe-icon-7-stroke.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/fonts/feather.css", CONF_VIEW_ADMIN); ?>">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="<?= theme("/assets/css/bootstrap-material.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/css/shreerang-material.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/css/uikit.css", CONF_VIEW_ADMIN); ?>">

    <!-- Libs -->
    <link rel="stylesheet" href="<?= theme("/assets/libs/perfect-scrollbar/perfect-scrollbar.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/libs/flot/flot.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/libs/datatables/datatables.css", CONF_VIEW_ADMIN); ?>">
    
    <!-- Custom -->
    <link rel="stylesheet" href="<?= url("/shared/styles/datetimepicker.css"); ?>"/>
    <link rel="stylesheet" href="<?= theme("/assets/styles/style.css", CONF_VIEW_ADMIN); ?>">

    <!-- Pages -->
    <?= $v->section("pages"); ?>

</head>

<body>
    <!-- [ Preloader ] Start -->
    <div class="ajax_load" style="z-index: 9999;">
        <div class="ajax_load_box">
            <div class="ajax_load_box_circle"></div>
            <p class="ajax_load_box_title">Aguarde, carregando...</p>
        </div>
    </div>
    <!-- [ Preloader ] End -->

    <!-- [ Mensagens ] Start -->
    <div class="ajax_response"><?= flash(); ?></div>
    <div class="overlay"></div>
    <!-- [ Mensagem ] End -->

    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white logo-dark">
                <!-- Brand demo (see assets/css/demo/demo.css) -->
                <div class="app-brand demo">
                    <span class="app-brand-logo demo" style="width:40px">
                        <img src="<?= theme("/assets/img/logoface.png", CONF_VIEW_ADMIN); ?>" alt="Fox Logo" class="img-fluid">
                    </span>
                    <a href="<?= url("/".PATH_ADMIN);?>" class="app-brand-text demo sidenav-text font-weight-normal ml-2">Fox</a>
                    <a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                        <i class="ion ion-md-menu align-middle"></i>
                    </a>
                </div>
                <div class="sidenav-divider mt-0"></div>

                <!-- Links -->
                <ul class="sidenav-inner py-1">

                    <?php
                        $nav = function ($icon, $href, $title, array $sub = null) use ($app) {
                            $active = (explode("/", $app)[0] == explode("/", $href)[0] ? "active" : null);
                            $open = (explode("/", $app)[0] == explode("/", $href)[0] ? "open" : null);
                            $url = url("/".PATH_ADMIN."/{$href}");
                            //verifica a permissão do usuário
                            // $permissoes = ['dash','blog','column','timer','agend','gal','ads','users','trash'];
                            $permissoes = explode(',',user()->permission);
                            if(!in_array(explode("/", $href)[0], $permissoes)){
                                if(strpos($_SERVER['REQUEST_URI'], explode("/", $href)[0]) !== false){
                                    redirect("/".PATH_ADMIN."/dash/home");
                                    exit;
                                }
                                return false;
                            }

                            if($sub != null){
                                $return = "<li class=\"sidenav-item {$open} {$active}\">";
                                $return .= "<a href=\"javascript:\" class=\"sidenav-link sidenav-toggle\"><i class=\"sidenav-icon feather icon-{$icon}\"></i><div>{$title}</div></a>";
                                    if($sub != null):
                                        $return .= "<ul class=\"sidenav-menu\">";
                                            $navSub = function ($titleSub, $hrefSub) use ($app) {
                                                $activeSub = ($app == $hrefSub ? "active" : null);
                                                $urlSub = url("/".PATH_ADMIN."/{$hrefSub}");
            
                                                $arrayUser = explode(',',user()->permission);
                                                $arrayPermission = (new Source\Models\Permission())->find()->fetch(true);
                                                $arrayPer = [];
                                                foreach($arrayPermission as $ap){
                                                    $arrayPer[] = $ap->uri; 
                                                }
            
                                                //compara os 2 array do banco permission com user permission
                                                $arrayResult = array_diff($arrayPer,$arrayUser);
                                                if(in_array(explode("/", $hrefSub)[1],$arrayResult)){
                                                    if(strpos($_SERVER['REQUEST_URI'], explode("/", $hrefSub)[1]) !== false){
                                                        redirect("/".PATH_ADMIN."/dash/home");
                                                        exit;
                                                    }
                                                    return false;
                                                }
            
                                                return "<li class=\"sidenav-item {$activeSub}\">
                                                        <a href=\"{$urlSub}\" class=\"sidenav-link\">
                                                            <div>• {$titleSub}</div>
                                                        </a>
                                                    </li>";
                                                
                                            };
                                            foreach($sub as $key => $value):
                                                $return .= $navSub($key,$value);
                                            endforeach;
                                        $return .="</ul>";
                                    endif;
                                $return .= "</li>";
                                return $return;
                            } else {
                                return "<li class=\"sidenav-item {$active}\"><a href=\"{$url}\" class=\"sidenav-link\"><i class=\"sidenav-icon feather icon-{$icon}\"></i><div>{$title}</div></a></li>";
                            }
                        };

                        $navFixed = function ($icon, $href, $title) use ($app) {
                            $active = (explode("/", $app)[0] == explode("/", $href)[0] ? "active" : null);
                            $url = url("/".PATH_ADMIN."/{$href}");
                            return "<li class=\"sidenav-item {$active}\"><a href=\"{$url}\" class=\"sidenav-link\"><i class=\"sidenav-icon feather icon-{$icon}\"></i><div>{$title}</div></a></li>";
                        };

                        //inicio do menu
                        echo $navFixed("home", "dash", "Painel");

                        $art = ["Listar"=>"blog/home","Categorias"=>"blog/categories","Novo Artigo"=>"blog/post"];
                        echo $nav("edit", "blog/home", "Artigos",$art);

                        $col = ["Listar"=>"column/home","Nova Coluna"=>"column/post"];
                        echo $nav("edit-1", "column/home", "Colunas", $col);
                        
                        $col = ["Listar"=>"pages/home","Adicionar"=>"pages/post"];
                        echo $nav("file-text", "pages/home", "Apresentação", $col);

                        $cron = ["Listar"=>"timer/home","Novo Cron"=>"timer/stopwatch"];
                        echo $nav("clock", "timer/home", "Cronômetro", $cron);

                        $event = ["Listar"=>"agend/home","Novo Evento"=>"agend/agenda"];
                        echo $nav("calendar", "agend/home", "Agenda de Eventos", $event);

                        $gale = ["Listar"=>"gal/home","Nova Galeria"=>"gal/gallery"];
                        echo $nav("camera", "gal/home", "Galerias", $gale);

                        $publ = ["Listar"=>"ads/home","Nova Publicidade"=>"ads/publicity"];
                        echo $nav("image", "ads/home", "Publicidades", $publ);

                        echo $nav("bar-chart-2", "faq/home", "Enquetes");

                        $clients = ["Listar"=>"clients/home","Add Cliente"=>"clients/client"];
                        echo $nav("user-plus","clients/home", "Clientes", $clients);

                        $cards = ["Listar"=>"cards/home","Add Cards"=>"cards/card"];
                        echo $nav("layers","cards/home", "Cards", $cards);

                        $shedule = ["Calendário"=>"shedule/home","Ver Lista"=>"shedule/list"];
                        echo $nav("calendar","shedule/home", "Agenda",$shedule);

                        $menu_usuario = ["Listar"=>"users/home","Novo Usuário"=>"users/user"];
                        echo $nav("users", "users/home", "Usuários", $menu_usuario);

                        echo $nav("trash-2", "trash/home", "Lixeira");

                        $configuracoes = ["Listar"=>"setting/home","Nova Permissão"=>"setting/permission"];
                        echo $nav("settings", "setting/home", "Configurações", $configuracoes);
                    ?>
                </ul>
            </div>
            <!-- [ Layout sidenav ] End -->
            
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] Start -->
                <nav class="layout-navbar navbar navbar-expand-lg align-items-lg-center bg-dark container-p-x" id="layout-navbar">

                    <!-- Brand demo (see assets/css/demo/demo.css) -->
                    <a href="<?= url("/".PATH_ADMIN);?>" class="navbar-brand app-brand demo d-lg-none py-0 mr-4">
                        <span class="app-brand-logo demo" style="width:40px">
                            <img src="<?= theme("/assets/img/logoface.png", CONF_VIEW_ADMIN); ?>" alt="Fox Logo" class="img-fluid">
                        </span>
                        <span class="app-brand-text demo font-weight-normal ml-2">Fox</span>
                    </a>

                    <!-- Sidenav toggle (see assets/css/demo/demo.css) -->
                    <div class="layout-sidenav-toggle navbar-nav d-lg-none align-items-lg-center mr-auto">
                        <a class="nav-item nav-link px-0 mr-lg-4" href="javascript:">
                            <i class="ion ion-md-menu text-large align-middle"></i>
                        </a>
                    </div>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#layout-navbar-collapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="navbar-collapse collapse" id="layout-navbar-collapse">
                        <!-- Divider -->
                        <hr class="d-lg-none w-100 my-2">

                        <div class="navbar-nav align-items-lg-center ml-auto">
                            <div class="demo-navbar-notifications nav-item dropdown mr-lg-3">
                                <a class="nav-link dropdown-toggle hide-arrow" href="#" data-toggle="dropdown">
                                    <i class="feather icon-bell navbar-icon align-middle"></i>
                                    <span class="badge badge-danger badge-dot indicator"></span>
                                    <span class="d-lg-none align-middle">&nbsp; Notifications</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="bg-primary text-center text-white font-weight-bold p-3">
                                        4 New Notifications
                                    </div>
                                    <div class="list-group list-group-flush">
                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-home bg-secondary border-0 text-white"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">Login from 192.168.1.1</div>
                                                <div class="text-light small mt-1">
                                                    Aliquam ex eros, imperdiet vulputate hendrerit et.
                                                </div>
                                                <div class="text-light small mt-1">12h ago</div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-user-plus bg-primary border-0 text-white"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">You have
                                                    <strong>4</strong> new followers</div>
                                                <div class="text-light small mt-1">
                                                    Phasellus nunc nisl, posuere cursus pretium nec, dictum vehicula tellus.
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-power bg-danger border-0 text-white"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">Server restarted</div>
                                                <div class="text-light small mt-1">
                                                    19h ago
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-alert-triangle bg-warning border-0 text-dark"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">99% server load</div>
                                                <div class="text-light small mt-1">
                                                    Etiam nec fringilla magna. Donec mi metus.
                                                </div>
                                                <div class="text-light small mt-1">
                                                    20h ago
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <a href="javascript:" class="d-block text-center text-light small p-2 my-1">Show all notifications</a>
                                </div>
                            </div>

                            <div class="demo-navbar-messages nav-item dropdown mr-lg-3">
                                <a class="nav-link dropdown-toggle hide-arrow" href="#" data-toggle="dropdown">
                                    <i class="feather icon-mail navbar-icon align-middle"></i>
                                    <span class="badge badge-success badge-dot indicator"></span>
                                    <span class="d-lg-none align-middle">&nbsp; Messages</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="bg-primary text-center text-white font-weight-bold p-3">
                                        4 New Messages
                                    </div>
                                    <div class="list-group list-group-flush">
                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <img src="assets/img/avatars/6-small.png" class="d-block ui-w-40 rounded-circle" alt>
                                            <div class="media-body ml-3">
                                                <div class="text-dark line-height-condenced">Lorem ipsum dolor consectetuer elit.</div>
                                                <div class="text-light small mt-1">
                                                    Josephin Doe &nbsp;·&nbsp; 58m ago
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <img src="assets/img/avatars/4-small.png" class="d-block ui-w-40 rounded-circle" alt>
                                            <div class="media-body ml-3">
                                                <div class="text-dark line-height-condenced">Lorem ipsum dolor sit amet, consectetuer.</div>
                                                <div class="text-light small mt-1">
                                                    Lary Doe &nbsp;·&nbsp; 1h ago
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <img src="assets/img/avatars/5-small.png" class="d-block ui-w-40 rounded-circle" alt>
                                            <div class="media-body ml-3">
                                                <div class="text-dark line-height-condenced">Lorem ipsum dolor sit amet elit.</div>
                                                <div class="text-light small mt-1">
                                                    Alice &nbsp;·&nbsp; 2h ago
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <img src="assets/img/avatars/11-small.png" class="d-block ui-w-40 rounded-circle" alt>
                                            <div class="media-body ml-3">
                                                <div class="text-dark line-height-condenced">Lorem ipsum dolor sit amet consectetuer amet elit dolor sit.</div>
                                                <div class="text-light small mt-1">
                                                    Suzen &nbsp;·&nbsp; 5h ago
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <a href="javascript:" class="d-block text-center text-light small p-2 my-1">Show all messages</a>
                                </div>
                            </div>

                            <!-- Divider -->
                            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|</div>
                            <div class="demo-navbar-user nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                                    <?php
                                        $photo = user()->photo();
                                        $userPhoto = ($photo ? image($photo, 50, 50) : theme("/assets/img/avatar.jpg", CONF_VIEW_ADMIN));
                                    ?>
                                    <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                                        <img src="<?=$userPhoto;?>" alt class="d-block ui-w-30 rounded-circle">
                                        <span class="px-1 mr-lg-2 ml-2 ml-lg-0"><?= user()->fullName(); ?></span>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?= url("/".PATH_ADMIN."/users/user/" . user()->id); ?>" class="dropdown-item">
                                        <i class="feather icon-user text-muted"></i> &nbsp; Meu Perfil</a>
                                    <a href="javascript:" class="dropdown-item">
                                        <i class="feather icon-mail text-muted"></i> &nbsp; Messagem</a>
                                    <a href="<?=url("/".PATH_ADMIN."/logoff");?>" class="dropdown-item">
                                        <i class="feather icon-power text-danger"></i> &nbsp; Sair</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- [ Layout navbar ( Header ) ] End -->

                <!-- [ Layout content ] Start -->
                <div class="layout-content">

                    <!-- [ content ] Start -->
                    <?= $v->section("content"); ?>
                    <!-- [ content ] End -->

                    <!-- [ Layout footer ] Start -->
                    <nav class="layout-footer footer footer-light">
                        <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
                            <div class="pt-3">
                                <span class="float-md-right d-none d-lg-block">&copy; Estudio Fox <a href="https://www.estudiofox.com" target="_blank"><img src="<?=theme("/assets/img/logo-fox.png",CONF_VIEW_ADMIN);?>" alt="Fox" height="20"/></a> | Feito à mão &amp; Feito com <i class="fas fa-heart text-danger mr-2"></i></span>
                            </div>
                            <div>
                                <a href="javascript:" class="footer-link pt-3">Sobre Nós</a>
                                <a href="javascript:" class="footer-link pt-3 ml-4">Ajuda</a>
                                <a href="javascript:" class="footer-link pt-3 ml-4">Contato</a>
                                <a href="javascript:" class="footer-link pt-3 ml-4">Termos &amp; Condições</a>
                            </div>
                        </div>
                    </nav>
                    <!-- [ Layout footer ] End -->
                </div>
                <!-- [ Layout content ] End -->
            </div>
            <!-- [ Layout container ] End -->
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->

    <!-- Core scripts -->
    <script src="<?= theme("/assets/js/pace.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/js/jquery-3.3.1.min.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/libs/popper/popper.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/js/bootstrap.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/js/sidenav.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/js/layout-helpers.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/js/material-ripple.js", CONF_VIEW_ADMIN); ?>"></script>

    <!-- Custom Enos -->
    <script src="<?= url("/shared/scripts/jquery.form.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/jquery-ui.js"); ?>"></script>

    <!-- Libs -->
    <script src="<?= theme("/assets/libs/perfect-scrollbar/perfect-scrollbar.js", CONF_VIEW_ADMIN); ?>"></script>

    <!-- Menu -->
    <script src="<?= theme("/assets/js/demo.min.js", CONF_VIEW_ADMIN); ?>"></script>
    
    <!-- Scripts Page -->
    <script src="<?= theme("/assets/scripts/scripts.js", CONF_VIEW_ADMIN); ?>"></script>
    <?= $v->section("scripts"); ?>

</body>

</html>
