<!DOCTYPE html>
<html lang="pt-br" class="default-style layout-fixed layout-navbar-fixed">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?= $head; ?>

    <meta name="author" content="Enos.Fox" />
    <link rel="icon" type="image/x-icon" href="<?= theme("/assets/img/favicon.ico", CONF_VIEW_ADMIN); ?>">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?= theme("/assets/fonts/feather.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/fonts/fontawesome.css", CONF_VIEW_ADMIN); ?>">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="<?= theme("/assets/css/bootstrap-material.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/css/shreerang-material.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/css/uikit.css", CONF_VIEW_ADMIN); ?>">

    <!-- Page -->
    <link rel="stylesheet" href="<?= theme("/assets/css/pages/authentication.css", CONF_VIEW_ADMIN); ?>">

    <!-- Custom -->
    <link rel="stylesheet" href="<?= theme("/assets/styles/style.css", CONF_VIEW_ADMIN); ?>">

</head>
<body>

    <!-- [ Preloader ] Start -->
    <div class="ajax_load">
        <div class="ajax_load_box">
            <div class="ajax_load_box_circle"></div>
            <p class="ajax_load_box_title">Aguarde, carregando...</p>
        </div>
    </div>
    <!-- [ Preloader ] End -->

    <!-- [ content ] Start -->
    <div class="authentication-wrapper authentication-3">
        <div class="authentication-inner">

        <?= $v->section("content"); ?>

        </div>
    </div>
    <!-- [ content ] End -->

    <!-- Core scripts -->
    <script src="<?= theme("/assets/js/jquery-3.3.1.min.js", CONF_VIEW_ADMIN); ?>"></script>
    <script src="<?= theme("/assets/js/bootstrap.js", CONF_VIEW_ADMIN); ?>"></script>

    <!-- Custom scripts -->
    <script src="<?= theme("/assets/scripts/login.js", CONF_VIEW_ADMIN); ?>"></script>

</body>
</html>