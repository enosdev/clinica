<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class Client Active Record Pattern
 *
 * @author Enos S. Sousa <sac@estudiofox.com>
 * @package Source\Models
 */
class Client extends Model
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct("clients", ["id"], ["first_name", "last_name", "document"]);
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $document
     * @return Client
     */
    public function bootstrap(
        string $firstName,
        string $lastName,
        string $document
    ): Client {
        $this->first_name = $firstName;
        $this->last_name = $lastName;
        $this->document = $document;
        return $this;
    }

    /**
     * @param string $document
     * @param string $columns
     * @return null|Client
     */
    public function findByDocument(string $document, string $columns = "*"): ?Client
    {
        $find = $this->find("document = :document", "document={$document}", $columns);
        return $find->fetch();
    }

    /**
     * @return string
     */
    public function fullName(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return null|Card
     */
    public function card(): ?Card
    {
        if ($this->id) {
            return (new Card())->find("client = :id", "id={$this->id}")->fetch();
        }
        return null;
    }


    /**
     * @return string|null
     */
    public function photo(): ?string
    {
        if ($this->photo_client && file_exists(__DIR__ . "/../../" . CONF_UPLOAD_DIR . "/{$this->photo_client}")) {
            return $this->photo_client;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Nome, Sobrenome, CPF são obrigatórios");
            return false;
        }

        /** Client Update */
        if (!empty($this->id)) {
            $userId = $this->id;

            if ($this->find("document = :d AND id != :i", "d={$this->document}&i={$userId}", "id")->fetch()) {
                $this->message->warning("O CPF informado já está cadastrado");
                return false;
            }

            $this->update($this->safe(), "id = :id", "id={$userId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Client Create */
        if (empty($this->id)) {
            if ($this->findByDocument($this->document, "id")) {
                $this->message->warning("O CPF informado já está cadastrado");
                return false;
            }

            $userId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($userId))->data();
        return true;
    }
}