<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class User Active Record Pattern
 *
 * @author Enos S. Sousa <sac@estudiofox.com>
 * @package Source\Models
 */
class Shedule extends Model
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct("shedule", ["id"], ["user", "card", "sheduled_at"]);
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return mixed|Model
     */
    public function findShedule(?string $terms = null, ?string $params = null, string $columns = "*")
    {
        $terms = "status = :status" . ($terms ? " AND {$terms}" : "");
        $params = "status=post" . ($params ? "&{$params}" : "");

        return parent::find($terms, $params, $columns);
    }

    /**
     * @return null|User
     */
    public function doctor(): ?User
    {
        if ($this->user) {
            return (new User())->findById($this->user);
        }
        return null;
    }

    /**
     * @return null|Card
     */
    public function card(): ?Card
    {
        if ($this->card) {
            return (new Card())->findById($this->card);
        }
        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Dentista e Horário são obrigatórios");
            return false;
        }

        /** Shedule Update */
        if (!empty($this->id)) {
            $sheduleId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$sheduleId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Shedule Create */
        if (empty($this->id)) {

            $sheduleId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($sheduleId))->data();
        return true;
    }
}