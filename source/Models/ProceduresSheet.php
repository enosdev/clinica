<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class ProceduresSheet Active Record Pattern
 *
 * @author Enos S. Sousa <sac@estudiofox.com>
 * @package Source\Models
 */
class ProceduresSheet extends Model
{
    /**
     * Card constructor.
     */
    public function __construct()
    {
        parent::__construct("procedures_sheet", ["id"], []);
    }

    /**
     * @param string $card
     * @param string $columns
     * @return null|ProceduresSheet
     */
    public function findByProceduresSheet(string $card, string $columns = "*"): ?ProceduresSheet
    {
        $find = $this->find("card = :card", "card={$card}", $columns);
        return $find->fetch();
    }

    /**
     * @return null|Procedure
     */
    public function procedure(): ?Procedure
    {
        if ($this->procedures) {
            return (new Procedure())->findById($this->procedures);
        }
        return null;
    }

    /**
     * @return null|Shedule
     */
    public function sheduleCardProcedure(): ?Shedule
    {
        if ($this->card) {
            return (new Shedule())->find("card = :c && procedure_sheet = :p","c={$this->card}&p={$this->id}")->fetch();
        }
        return null;
    }

    /**
     * @return null|User
     */
    public function doctor(): ?User
    {
        if ($this->sheduleCardProcedure()) {
            return (new User())->findById($this->sheduleCardProcedure()->user);
        }
        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Procedimentos, Card e Valor são obrigatórios");
            return false;
        }

        /** Procedure Sheet Update */
        if (!empty($this->id)) {
            $procSheeId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$procSheeId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Procedure Sheet Create */
        if (empty($this->id)) {

            $procSheeId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($procSheeId))->data();
        return true;
    }
}