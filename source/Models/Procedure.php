<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * Class Notification
 * @package Source\Models
 */
class Procedure extends Model
{
    /**
     * Notification constructor.
     */
    public function __construct()
    {
        parent::__construct("procedures", ["id"], ["name", "value"]);
    }
}