<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class Cards Active Record Pattern
 *
 * @author Enos S. Sousa <sac@estudiofox.com>
 * @package Source\Models
 */
class Card extends Model
{
    /**
     * Card constructor.
     */
    public function __construct()
    {
        parent::__construct("cards", ["id"], ["client"]);
    }

    /**
     * @param string $client
     * @return Cards
     */
    public function bootstrap(
        string $client
    ): Client {
        $this->client = $client;
        return $this;
    }

    /**
     * @param string $client
     * @param string $columns
     * @return null|Cards
     */
    public function findByClient(string $client, string $columns = "*"): ?Cards
    {
        $find = $this->find("client = :client", "client={$client}", $columns);
        return $find->fetch();
    }

    /**
     * @return null|Client
     */
    public function client(): ?Client
    {
        if ($this->client) {
            return (new Client())->findById($this->client);
        }
        return null;
    }

    /**
     * @return null|Shedule
     */
    public function sheduleCard(): ?Shedule
    {
        if ($this->id) {
            return (new Shedule())->findById($this->id);
        }
        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Cliente e Usuário são obrigatórios");
            return false;
        }

        /** Card Update */
        if (!empty($this->id)) {
            $cardId = $this->id;

            if ($this->find("client = :d AND id != :i", "d={$this->client}&i={$cardId}", "id")->fetch()) {
                $this->message->warning("O cliente já contem ficha cadastrada");
                return false;
            }

            $this->update($this->safe(), "id = :id", "id={$cardId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Card Create */
        if (empty($this->id)) {
            if ($this->findByClient($this->client, "id")) {
                $this->message->warning("O cliente já contem ficha cadastrada");
                return false;
            }

            $cardId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($cardId))->data();
        return true;
    }
}