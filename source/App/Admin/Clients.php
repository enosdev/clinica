<?php

namespace Source\App\Admin;

use Source\Models\Client;
use Source\Models\Permission;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Clients
 * @package Source\App\Admin
 */
class Clients extends Admin
{
    /**
     * Clients constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home/{$s}/1")]);
            return;
        }

        $search = null;
        $clients = (new Client())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $clients = (new Client())->find("MATCH(first_name, last_name, document) AGAINST(:s)", "s={$search}");

            if (!$clients->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/clients/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/clients/home/{$all}/"));
        $pager->pager($clients->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Clientes",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/clients/home", [
            "app" => "clients/home",
            "head" => $head,
            "search" => $search,
            "count" => $clients->count(),
            "clients" => $clients->order("first_name, last_name")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function client(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $description = $data["description"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $clientCreate = new Client();
            $clientCreate->first_name = $data["first_name"];
            $clientCreate->last_name = $data["last_name"];
            $clientCreate->datebirth = date_fmt_back($data["datebirth"]);
            $clientCreate->genre = $data["genre"];
            $clientCreate->document = preg_replace("/[^0-9]/", "", $data["document"]);
            $clientCreate->identity = $data["identity"];
            $clientCreate->phone = $data["phone"];
            $clientCreate->phone2 = $data["phone2"];
            $clientCreate->email = $data["email"];
            $clientCreate->instagram = $data["instagram"];
            $clientCreate->facebook = $data["facebook"];
            $clientCreate->description = $description;
            $clientCreate->id_user = $data["id_user"];

            //upload photo client
            if (!empty($_FILES["photo_client"])) {
                $files1 = $_FILES["photo_client"];
                $upload1 = new Upload();
                $image1 = $upload1->image($files1, $clientCreate->fullName().'-face', 600);

                if (!$image1) {
                    $json["message"] = $upload1->message()->render();
                    echo json_encode($json);
                    return;
                }

                $clientCreate->photo_client = $image1;
            }

            //upload photo
            if (!empty($_FILES["photo"])) {
                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $clientCreate->fullName().'-frente', 600);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $clientCreate->photo = $image;
            }
            //upload photo2
            if (!empty($_FILES["photo2"])) {
                $files2 = $_FILES["photo2"];
                $upload2 = new Upload();
                $image2 = $upload2->image($files2, $clientCreate->fullName().'-verso', 600);

                if (!$image2) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $clientCreate->photo2 = $image2;
            }

            if (!$clientCreate->save()) {
                $json["message"] = $clientCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Usuário cadastrado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/clients/client/{$clientCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $description = $data["description"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $clientUpdate = (new Client())->findById($data["client_id"]);

            if (!$clientUpdate) {
                $this->message->error("Você tentou gerenciar um client que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);
                return;
            }

            $clientUpdate->first_name = $data["first_name"];
            $clientUpdate->last_name = $data["last_name"];
            $clientUpdate->datebirth = date_fmt_back($data["datebirth"]);
            $clientUpdate->genre = $data["genre"];
            $clientUpdate->document = preg_replace("/[^0-9]/", "", $data["document"]);
            $clientUpdate->identity = $data["identity"];
            $clientUpdate->phone = $data["phone"];
            $clientUpdate->phone2 = $data["phone2"];
            $clientUpdate->email = $data["email"];
            $clientUpdate->instagram = $data["instagram"];
            $clientUpdate->facebook = $data["facebook"];
            $clientUpdate->description = $description;

            //upload photo client
            if (!empty($_FILES["photo_client"])) {
                if ($clientUpdate->photo_client && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientUpdate->photo_client}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientUpdate->photo_client}");
                    (new Thumb())->flush($clientUpdate->photo_client);
                }

                $files_client = $_FILES["photo_client"];
                $upload_client = new Upload();
                $image_client = $upload_client->image($files_client, $clientUpdate->fullName().'-face', 600);

                if (!$image_client) {
                    $json["message"] = $upload_client->message()->render();
                    echo json_encode($json);
                    return;
                }

                $clientUpdate->photo_client = $image_client;
            }

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($clientUpdate->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientUpdate->photo}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientUpdate->photo}");
                    (new Thumb())->flush($clientUpdate->photo);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $clientUpdate->fullName().'-frente', 600);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $clientUpdate->photo = $image;
            }

            //upload photo2
            if (!empty($_FILES["photo2"])) {
                if ($clientUpdate->photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientUpdate->photo2}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientUpdate->photo2}");
                    (new Thumb())->flush($clientUpdate->photo2);
                }

                $files2 = $_FILES["photo2"];
                $upload2 = new Upload();
                $image2 = $upload2->image($files2, $clientUpdate->fullName().'-verso', 600);

                if (!$image2) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $clientUpdate->photo2 = $image2;
            }

            if (!$clientUpdate->save()) {
                $json["message"] = $clientUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Cliente atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $clientDelete = (new Client())->findById($data["client_id"]);

            if (!$clientDelete) {
                $this->message->error("Você tentou deletar um Cliente que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);
                return;
            }

            if ($clientDelete->photo_client && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo_client}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo_client}");
                (new Thumb())->flush($clientDelete->photo_client);
            }

            if ($clientDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo}");
                (new Thumb())->flush($clientDelete->photo);
            }

            if ($clientDelete->photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo2}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo2}");
                (new Thumb())->flush($clientDelete->photo2);
            }

            $clientDelete->destroy();

            $this->message->success("O cliente foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);

            return;
        }

        $clientEdit = null;
        if (!empty($data["client_id"])) {
            $clientId = filter_var($data["client_id"], FILTER_VALIDATE_INT);
            $clientEdit = (new Client())->findById($clientId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($clientEdit ? "Perfil de {$clientEdit->fullName()}" : "Novo Cliente"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/clients/client", [
            "app" => "clients/client",
            "head" => $head,
            "clients" => $clientEdit
        ]);
    }
}