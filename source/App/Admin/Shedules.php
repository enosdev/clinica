<?php

namespace Source\App\Admin;

use Source\Models\Card;
use Source\Models\User;
use Source\Models\Shedule;
use Source\Models\Client;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Clients
 * @package Source\App\Admin
 */
class Shedules extends Admin
{
    /**
     * Clients constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //lista horários dos dentistas
        if (!empty($data["shedule"])) {
            $list = null;
            $shedule = (new Shedule())->find("card = :card","card={$data['card']}")->fetch();
            $card = (new Card())->findById($data['card']);
            
            echo json_encode([
                "doctor" => $shedule->doctor()->first_name . ' ' . $shedule->doctor()->last_name,
                "type" => ($shedule->type == 'budget')? 'Orçamento' : 'Procedimento',
                "day" => date_fmt($shedule->sheduled_at, "d/m/Y \à\s H:i"),
                "day_end" => ($shedule->attendance_at != null)? date_fmt($shedule->attendance_at, "d/m/Y \à\s H:i") : null ,
                "client" => $card->client()->first_name . ' ' . $card->client()->last_name
            ]);
            return;
        }

        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/cards/home/{$s}/1")]);
            return;
        }

        $search = null;
        $cards = (new Card())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $clientSearch = (new Client())->find("MATCH(first_name, last_name) AGAINST(:s)", "s={$search}");

            if (!$clientSearch->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/cards/home");
            }

            foreach($clientSearch->fetch(true) as $searching){
                $res[] = $searching->id;
            }
            $rf = implode(',', $res);

            $cards = (new Card())->find("client IN({$rf})");

            if (!$cards->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/cards/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/cards/home/{$all}/"));
        $pager->pager($cards->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $shedule = (new Shedule())->findShedule()->fetch(true);

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Fichas",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/shedule/home", [
            "app" => "shedule/home",
            "head" => $head,
            "shedule" => $shedule
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function card(?array $data): void
    {
        //lista horários dos dentistas
        if (!empty($data["refresh"])) {
            $list = null;
            $items = (new Shedule())->find("user = :user","user={$data['user']}")->order("sheduled_at")->fetch(true);
            if ($items) {
                foreach ($items as $item) {
                    $list[] = [
                        "sheduled_at" => date_fmt($item->sheduled_at,"d/m/Y \à\s H:i")
                    ];
                }
            }

            echo json_encode([
                "list" => $list
            ]);
            return;
        }

        //create shedule budget
        if (!empty($data["action"]) && $data["action"] == "create_budget") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $cardCreate = new Shedule();
            $cardCreate->user = $data["user"];
            $cardCreate->card = $data["card"];
            $cardCreate->sheduled_at = date_fmt_back($data["sheduled_at"]);
            $cardCreate->type = "budget";
            $cardCreate->status = "post";

            if (!$cardCreate->save()) {
                $json["message"] = $cardCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Agendamento enviado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/cards/card/{$cardCreate->id}");

            echo json_encode($json);
            return;
        }

        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $cardCreate = new Card();
            $cardCreate->client = $data["client"];
            $cardCreate->user = user()->id;

            if (!$cardCreate->save()) {
                $json["message"] = $cardCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Ficha criada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/cards/card/{$cardCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $description = $data["description"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $cardUpdate = (new Card())->findById($data["card_id"]);

            if (!$cardUpdate) {
                $this->message->error("Você tentou gerenciar um client que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);
                return;
            }

            $cardUpdate->first_name = $data["first_name"];
            $cardUpdate->last_name = $data["last_name"];
            $cardUpdate->datebirth = date_fmt_back($data["datebirth"]);
            $cardUpdate->genre = $data["genre"];
            $cardUpdate->document = preg_replace("/[^0-9]/", "", $data["document"]);
            $cardUpdate->identity = $data["identity"];
            $cardUpdate->phone = $data["phone"];
            $cardUpdate->phone2 = $data["phone2"];
            $cardUpdate->email = $data["email"];
            $cardUpdate->instagram = $data["instagram"];
            $cardUpdate->facebook = $data["facebook"];
            $cardUpdate->description = $description;
            $cardUpdate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($cardUpdate->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo}");
                    (new Thumb())->flush($cardUpdate->photo);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $cardUpdate->fullName().'-frente', 600);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $cardUpdate->photo = $image;
            }

            //upload photo2
            if (!empty($_FILES["photo2"])) {
                if ($cardUpdate->photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo2}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo2}");
                    (new Thumb())->flush($cardUpdate->photo2);
                }

                $files2 = $_FILES["photo2"];
                $upload2 = new Upload();
                $image2 = $upload2->image($files2, $cardUpdate->fullName().'-verso', 600);

                if (!$image2) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $cardUpdate->photo2 = $image2;
            }

            if (!$cardUpdate->save()) {
                $json["message"] = $cardUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Cliente atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $clientDelete = (new Card())->findById($data["card_id"]);

            if (!$clientDelete) {
                $this->message->error("Você tentou deletar um Cliente que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);
                return;
            }

            if ($clientDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo}");
                (new Thumb())->flush($clientDelete->photo);
            }

            if ($clientDelete->photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo2}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo2}");
                (new Thumb())->flush($clientDelete->photo2);
            }

            $clientDelete->destroy();

            $this->message->success("O cliente foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);

            return;
        }

        $cardEdit = null;
        if (!empty($data["card_id"])) {
            $cardId = filter_var($data["card_id"], FILTER_VALIDATE_INT);
            $cardEdit = (new Card())->findById($cardId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($cardEdit ? "Perfil de {$cardEdit->client()->first_name}" : "Novo Cliente"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/cards/card", [
            "app" => "cards/card",
            "head" => $head,
            "cards" => $cardEdit,
            "doctor" => (new User())->find("level = :level","level=8")->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     */
    public function list(?array $data): void
    {
        //lista horários dos dentistas
        if (!empty($data["shedule"])) {
            $list = null;
            $shedule = (new Shedule())->find("card = :card","card={$data['card']}")->fetch();
            $card = (new Card())->findById($data['card']);
            
            echo json_encode([
                "doctor" => $shedule->doctor()->first_name . ' ' . $shedule->doctor()->last_name,
                "type" => ($shedule->type == 'budget')? 'Orçamento' : 'Procedimento',
                "day" => date_fmt($shedule->sheduled_at, "d/m/Y \à\s H:i"),
                "day_end" => ($shedule->attendance_at != null)? date_fmt($shedule->attendance_at, "d/m/Y \à\s H:i") : null ,
                "client" => $card->client()->first_name . ' ' . $card->client()->last_name
            ]);
            return;
        }

        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/cards/home/{$s}/1")]);
            return;
        }

        $search = null;
        $cards = (new Card())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $clientSearch = (new Client())->find("MATCH(first_name, last_name) AGAINST(:s)", "s={$search}");

            if (!$clientSearch->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/cards/home");
            }

            foreach($clientSearch->fetch(true) as $searching){
                $res[] = $searching->id;
            }
            $rf = implode(',', $res);

            $cards = (new Card())->find("client IN({$rf})");

            if (!$cards->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/cards/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/cards/home/{$all}/"));
        $pager->pager($cards->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $list = (new Shedule())->findShedule()->order("sheduled_at")->fetch(true);

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Fichas",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/shedule/list", [
            "app" => "shedule/list",
            "head" => $head,
            "list" => $list
        ]);
    }
}