<?php

namespace Source\App\Admin;

use Source\Models\Card;
use Source\Models\User;
use Source\Models\Shedule;
use Source\Models\ProceduresSheet;
use Source\Models\Client;
use Source\Models\Procedure;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Clients
 * @package Source\App\Admin
 */
class Cards extends Admin
{
    /**
     * Clients constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/cards/home/{$s}/1")]);
            return;
        }

        $search = null;
        $cards = (new Card())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $clientSearch = (new Client())->find("MATCH(first_name, last_name) AGAINST(:s)", "s={$search}");

            if (!$clientSearch->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/cards/home");
            }

            foreach($clientSearch->fetch(true) as $searching){
                $res[] = $searching->id;
            }
            $rf = implode(',', $res);

            $cards = (new Card())->find("client IN({$rf})");

            if (!$cards->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/cards/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/cards/home/{$all}/"));
        $pager->pager($cards->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Fichas",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/cards/home", [
            "app" => "cards/home",
            "head" => $head,
            "search" => $search,
            "cards" => $cards->order("registry DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function card(?array $data): void
    {
        //lista horários dos dentistas
        if (!empty($data["refresh"])) {
            $list = null;
            $items = (new Shedule())->find("user = :user","user={$data['user']}")->order("sheduled_at")->fetch(true);
            if ($items) {
                foreach ($items as $item) {
                    $list[] = [
                        "sheduled_at" => date_fmt($item->sheduled_at,"d/m/Y \à\s H:i")
                    ];
                }
            }

            echo json_encode([
                "list" => $list
            ]);
            return;
        }

        //create shedule procedure
        if (!empty($data["action"]) && $data["action"] == "create_shedule_procedure") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $cardCreate = new Shedule();
            $cardCreate->user = $data["user"];
            $cardCreate->card = $data["card"];
            $cardCreate->sheduled_at = date_fmt_back($data["sheduled_at"]);
            $cardCreate->type = "procedure";
            $cardCreate->procedure_sheet = $data["procedure"];
            $cardCreate->status = "post";

            if (!$cardCreate->save()) {
                $json["message"] = $cardCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Agendamento enviado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/cards/card/{$cardCreate->card}");

            echo json_encode($json);
            return;
        }

        //create shedule budget
        if (!empty($data["action"]) && $data["action"] == "create_budget") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $cardCreate = new Shedule();
            $cardCreate->user = $data["user"];
            $cardCreate->card = $data["card"];
            $cardCreate->sheduled_at = date_fmt_back($data["sheduled_at"]);
            $cardCreate->type = "budget";
            $cardCreate->status = "post";

            if (!$cardCreate->save()) {
                $json["message"] = $cardCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Agendamento enviado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/cards/card/{$cardCreate->id}");

            echo json_encode($json);
            return;
        }

        //update attendance
        if (!empty($data["action"]) && $data["action"] == "update_atendimento") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $cardUpdate = (new Card())->findById($data["card_id"]);

            if (!$cardUpdate) {
                $this->message->error("Você tentou gerenciar um card que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/cards/home")]);
                return;
            }

            $cardUpdate->status = $data["status"];

            if (!$cardUpdate->save()) {
                $json["message"] = $cardUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $sheduleUpdate = (new Shedule())->findById($data["shedule_id"]);

            $sheduleUpdate->attendance_at = date("Y-m-d H:s:i");

            if (!$sheduleUpdate->save()) {
                $json["message"] = $sheduleUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Atendimento iniciado sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //start attendance
        if (!empty($data["action"]) && $data["action"] == "start_atendimento") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $sheduleUpdate = (new Shedule())->findById($data["shedule_id"]);

            $sheduleUpdate->attendance_at = date("Y-m-d H:s:i");

            if (!$sheduleUpdate->save()) {
                $json["message"] = $sheduleUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Atendimento iniciado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //stop attendance
        if (!empty($data["action"]) && $data["action"] == "stop_atendimento") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $sheduleUpdate = (new Shedule())->findById($data["shedule_id"]);

            $sheduleUpdate->completed_service = date("Y-m-d H:s:i");

            if (!$sheduleUpdate->save()) {
                $json["message"] = $sheduleUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $procedureSheetUpdate = (new ProceduresSheet())->findById($data['procedures_sheet_id']);
            $procedureSheetUpdate->status = "post";

            if (!$procedureSheetUpdate->save()) {
                $json["message"] = $procedureSheetUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Atendimento finalizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            if($data["registry"] && !empty($data["registry"])){
                $desc = (new Card())->find("registry IS NOT NULL")->order("registry DESC")->fetch();
                if($desc){
                    $registry = ($data["registry"] < 26154)? $data["registry"] : $desc->registry + 1;
                } else {
                    $registry = 26154;
                }
            } else {
                $desc = (new Card())->find("registry IS NOT NULL")->order("registry DESC")->fetch();
                $registry = ($desc)? $desc->registry + 1 : 26154;
            }

            $cardCreate = new Card();
            $cardCreate->client = $data["client"];
            $cardCreate->registry = $registry;
            $cardCreate->user = user()->id;

            if (!$cardCreate->save()) {
                $json["message"] = $cardCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Ficha criada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/cards/card/{$cardCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $description = $data["description"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $cardUpdate = (new Card())->findById($data["card_id"]);

            if (!$cardUpdate) {
                $this->message->error("Você tentou gerenciar um client que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);
                return;
            }

            $cardUpdate->first_name = $data["first_name"];
            $cardUpdate->last_name = $data["last_name"];
            $cardUpdate->datebirth = date_fmt_back($data["datebirth"]);
            $cardUpdate->genre = $data["genre"];
            $cardUpdate->document = preg_replace("/[^0-9]/", "", $data["document"]);
            $cardUpdate->identity = $data["identity"];
            $cardUpdate->phone = $data["phone"];
            $cardUpdate->phone2 = $data["phone2"];
            $cardUpdate->email = $data["email"];
            $cardUpdate->instagram = $data["instagram"];
            $cardUpdate->facebook = $data["facebook"];
            $cardUpdate->description = $description;
            $cardUpdate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($cardUpdate->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo}");
                    (new Thumb())->flush($cardUpdate->photo);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $cardUpdate->fullName().'-frente', 600);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $cardUpdate->photo = $image;
            }

            //upload photo2
            if (!empty($_FILES["photo2"])) {
                if ($cardUpdate->photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo2}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$cardUpdate->photo2}");
                    (new Thumb())->flush($cardUpdate->photo2);
                }

                $files2 = $_FILES["photo2"];
                $upload2 = new Upload();
                $image2 = $upload2->image($files2, $cardUpdate->fullName().'-verso', 600);

                if (!$image2) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $cardUpdate->photo2 = $image2;
            }

            if (!$cardUpdate->save()) {
                $json["message"] = $cardUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Cliente atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $clientDelete = (new Card())->findById($data["card_id"]);

            if (!$clientDelete) {
                $this->message->error("Você tentou deletar um Cliente que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);
                return;
            }

            if ($clientDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo}");
                (new Thumb())->flush($clientDelete->photo);
            }

            if ($clientDelete->photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo2}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$clientDelete->photo2}");
                (new Thumb())->flush($clientDelete->photo2);
            }

            $clientDelete->destroy();

            $this->message->success("O cliente foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/clients/home")]);

            return;
        }

        $cardEdit = null;
        if (!empty($data["card_id"])) {
            $cardId = filter_var($data["card_id"], FILTER_VALIDATE_INT);
            $cardEdit = (new Card())->findById($cardId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($cardEdit ? "Perfil de {$cardEdit->client()->first_name}" : "Novo Cliente"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/cards/card", [
            "app" => "cards/card",
            "head" => $head,
            "cards" => $cardEdit,
            "doctor" => (new User())->find("level = :level","level=8")->fetch(true),
            "procedures" => (new Procedure())->find()->fetch(true),
            "proSheet" => (new ProceduresSheet())->find("card = :c","c={$cardEdit->id}")->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function modalPost(?array $data): void
    {
        //create procedures_sheet
        if (!empty($data["action"]) && $data["action"] == "create_procedures_sheet") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $procedure_value = $data["procedure_value"];
            foreach($procedure_value as $key => $pv):
                $sheetCreate = new ProceduresSheet();
                $exp = explode('|',$pv);
                $sheetCreate->procedures = current($exp);
                $sheetCreate->value = end($exp);

                $sheetCreate->tooth = $data["tooth"][$key] == 99 ? NULL : $data["tooth"][$key] ;
                // $sheetCreate->note = $data["note"];

                $sheetCreate->user = $data["user"];
                $sheetCreate->card = $data["card"];
                $sheetCreate->status = 'draft';

                if (!$sheetCreate->save()) {
                    $json["message"] = $sheetCreate->message()->render();
                    echo json_encode($json);
                    return;
                }
                sleep(0.5);
            endforeach;

            $this->message->success("Procedimentos cadastrados com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/cards/card/{$data["card"]}");

            echo json_encode($json);
            return;
        }
    }
}