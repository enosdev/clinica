<?php

namespace Source\App\Admin;

use Source\Models\Publicity;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Ads extends Admin
{
    /**
     * Users constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/ads/home/{$s}/1")]);
            return;
        }

        $search = null;
        $ads = (new Publicity())->find("status != :status","status=trash");

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $ads = (new Publicity())->find("MATCH(name) AGAINST(:s)", "s={$search}");
            if (!$ads->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/ads/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/ads/home/{$all}/"));
        $pager->pager($ads->count(), 36, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Publicidades",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/ads/home", [
            "app" => "ads/home",
            "head" => $head,
            "search" => $search,
            "ads" => $ads->order("name, publicity_at")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function publicity(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $publicityCreate = new Publicity();
            $publicityCreate->name = $data["name"];
            $publicityCreate->link = (empty($data["link"]))? '#' : $data["link"];
            $publicityCreate->page = implode(',',$data["page"]);
            $publicityCreate->local = implode(',',$data["local"]);
            $publicityCreate->publicity_at = date_fmt_back($data["publicity_at"]);
            $publicityCreate->publicity_out = date_fmt_back($data["publicity_out"]);
            $publicityCreate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $publicityCreate->name, 1024);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $publicityCreate->photo = $image;
            }


            if (!$publicityCreate->save()) {
                $json["message"] = $publicityCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Publicidade cadastrada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/ads/publicity/{$publicityCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $publicityUpdate = (new Publicity())->findById($data["publicity_id"]);

            if (!$publicityUpdate) {
                $this->message->error("Você tentou gerenciar uma publicidade que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/ads/home")]);
                return;
            }

            $publicityUpdate->name = $data["name"];
            $publicityUpdate->link = (empty($data["link"]))? '#' : $data["link"];
            $publicityUpdate->page = implode(',',$data['page']);
            $publicityUpdate->local = implode(',',$data["local"]);
            $publicityUpdate->publicity_at = date_fmt_back($data["publicity_at"]);
            $publicityUpdate->publicity_out = date_fmt_back($data["publicity_out"]);
            $publicityUpdate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($publicityUpdate->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$publicityUpdate->photo}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$publicityUpdate->photo}");
                    (new Thumb())->flush($publicityUpdate->photo);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $publicityUpdate->name, 1024);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $publicityUpdate->photo = $image;
            }

            if (!$publicityUpdate->save()) {
                $json["message"] = $publicityUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Publicidade atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $publicityDelete = (new Publicity())->findById($data["publicity_id"]);

            if (!$publicityDelete) {
                $this->message->error("Você tentnou deletar uma publicidade que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/ads/home")]);
                return;
            }

            if ($publicityDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$publicityDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$publicityDelete->photo}");
                (new Thumb())->flush($publicityDelete->photo);
            }

            $publicityDelete->destroy();

            $this->message->success("A publicidade foi excluída com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/ads/home")]);

            return;
        }

        $publicityEdit = null;
        if (!empty($data["publicity_id"])) {
            $publicityId = filter_var($data["publicity_id"], FILTER_VALIDATE_INT);
            $publicityEdit = (new Publicity())->findById($publicityId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($publicityEdit ? "Publicidade de {$publicityEdit->name}" : "Nova Publicidade"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN.""),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/ads/publicity", [
            "app" => "ads/publicity",
            "head" => $head,
            "publicity" => $publicityEdit
        ]);
    }
}