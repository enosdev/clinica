<?php

function Name($Name)
{
    $Format = array();
    $Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
    $Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

    $Data = strtr(utf8_decode($Name), utf8_decode($Format['a']), $Format['b']);
    $Data = strip_tags(trim($Data));
    $Data = strtr($Data,' ','-');
    $Data = strtr($Data,['-----' => '-','----' => '-','---' => '-','--' => '-']);

    return strtolower(utf8_encode($Data));
}

$uploadDir = '../../storage/'.filter_input(1, 'name',FILTER_SANITIZE_STRING);
if(!is_dir($uploadDir)) {
    mkdir($uploadDir, 0777);
}

if (!empty($_FILES)) {
    $ext = explode('.', $_FILES['file']['name']);
    $arch = str_replace('.'.end($ext), '',$_FILES['file']['name']);

    $tmpFile = $_FILES['file']['tmp_name'];
    $filename = $uploadDir.'/'.time().'-'. Name($arch).'.'.Name(end($ext));
    move_uploaded_file($tmpFile,$filename);
}